#![no_std]

use core::fmt;

type Writer = unsafe extern "C" fn(*const u8, u32) -> u32;
pub struct DebugWriter(Writer);

extern {
    fn __rkos_write_debug(p: *const u8, n: u32) -> u32;
}

pub fn get_debug_writer() -> DebugWriter {
    DebugWriter(__rkos_write_debug)
}

impl fmt::Write for DebugWriter {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        let bytes = s.as_bytes();
        let out = unsafe {
            (self.0)(bytes.as_ptr(), bytes.len() as u32)
        };
        if out == bytes.len() as u32 {
            Ok(())
        } else {
            Err(fmt::Error)
        }
    }
}

#[macro_export]
macro_rules! debug_inner {
    ($($arg:tt)+) => ({
        use ::core::fmt::Write;
        let _ = write!($crate::get_debug_writer(), $($arg)+);
    })
}

/// Write formatted text to the debug console, similar to `println!`
#[macro_export]
macro_rules! debug {
    ($fmt:expr) => (debug_inner!(concat!(concat!(concat!(module_path!(), ": "), $fmt), "\n")));
    ($fmt:expr, $($arg:tt)*) => (debug_inner!(concat!(concat!(concat!(module_path!(), ": "), $fmt), "\n"), $($arg)*));
}
