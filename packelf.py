#!/usr/bin/env python3
import os, shutil, struct, sys

if len(sys.argv) < 4:
    print("Usage: {} <imagefile> <elf_file> <bin_file>".format(sys.argv[0]))
    sys.exit(1)

imagefile, elffile, binfile = sys.argv[1:4]

bin_size = os.stat(binfile).st_size
# Round size up to nearest sector
bin_size = (bin_size + 511) & ~(512 - 1)
assert bin_size % 512 == 0

with open(elffile, "rb") as elf:
    elf.seek(0x18)
    entrypoint = struct.unpack("<Q", elf.read(8))[0]
    print("Entry point is at", hex(entrypoint))

with open(imagefile, "rb+") as out:
    # Write entry point address
    out.seek(440 - 8)
    out.write(struct.pack("<Q", entrypoint))
    # Write length of partition 2
    out.seek(0x1da)
    out.write(struct.pack("<L", bin_size // 512))
    print("Partition size is", bin_size // 512, "sectors.")
