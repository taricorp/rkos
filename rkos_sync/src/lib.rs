#![feature(const_fn)]
#![no_std]

#[cfg(not(target_env=""))]
extern crate std;

extern crate rkos_thread as thread;

use core::cell::UnsafeCell;
use core::ops::{Deref, DerefMut};
use core::sync::atomic::{AtomicUsize, Ordering};

// If anything is going to use a thread ID when it's a pointer (in freestanding
// configurations), it must ensure such a thread exists. Reliable poisoning depends
// on having unwinding to drop live items.

pub struct Mutex<T>(AtomicUsize, UnsafeCell<T>);

pub struct MutexGuard<'a, T>(&'a Mutex<T>) where T: 'a;

pub type LockResult<'a, T> = Result<MutexGuard<'a, T>, LockError>;

/// Mutex is unlocked
const UNLOCKED: thread::Id = thread::Id::unlocked();
/// Mutex is locked but was locked outside a thread context (during system init)
const NOTHREAD: thread::Id = thread::Id::no_thread();
/// Mutex was locked by a thread that panicked.
const POISONED: thread::Id = thread::Id::poison();

#[derive(Debug, PartialEq, Eq)]
pub enum LockError {
    Locked,
    Poisoned
}

impl<T> Mutex<T> {
    pub const fn new(x: T) -> Mutex<T> {
        Mutex(AtomicUsize::new(UNLOCKED.0 as usize), UnsafeCell::new(x))
    }

    pub fn try_lock<'a>(&'a self) -> LockResult<'a, T> {
        fn id(tid: &thread::Id) -> usize {
            tid.0 as usize
        }

        let have_tid = thread::have_thread_context();
        let tid = if have_tid {
            thread::current_id()
        } else {
            NOTHREAD
        };
        let v = thread::Id(self.0.compare_and_swap(id(&UNLOCKED), id(&tid), Ordering::Acquire) as u32);

        if v == UNLOCKED {
            Ok(MutexGuard(self))
        } else if v == POISONED {
            Err(LockError::Poisoned)
        } else {
            if have_tid {
                // Error if trying to recursively acquire a lock.
                assert!(v != thread::current_id(), "Deadlock on mutex held by the current thread");
            }
            Err(LockError::Locked)
        }
    }

    pub fn lock<'a>(&'a self) -> MutexGuard<'a, T> {
        // Spin until we acquire.
        loop {
            match self.try_lock() {
                Ok(g) => return g,
                Err(LockError::Poisoned) =>
                    panic!("Attempted to acquire poisoned lock"),
                Err(LockError::Locked) => {
                    // TODO do something with the owning thread ID to try to reschedule if
                    // the lock's holder is not currently running.
                }
            }
        }
    }
}

impl<T: core::fmt::Debug> core::fmt::Debug for Mutex<T> {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        try!(write!(f, "Mutex("));
        try!(match self.0.load(Ordering::Acquire) {
            core::usize::MAX => write!(f, "poisoned"),
            0 => write!(f, "unlocked"),
            1 => write!(f, "locked but no thread context available"),
            x => write!(f, "locked by thread {:?}", x)
        });
        write!(f, " @ {:p})", self)
    }
}

impl<'a, T> core::fmt::Debug for MutexGuard<'a, T> {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        write!(f, "MutexGuard( Mutex @ {:p} )", self.0)
    }
}

unsafe impl<T: Send> Sync for Mutex<T> { }

impl<'a, T> Drop for MutexGuard<'a, T> {
    fn drop(&mut self) {
        let value = if thread::is_panicking() {
            POISONED
        } else {
            UNLOCKED
        };
        (self.0).0.store(value.0 as usize, Ordering::Release);
    }
}

impl<'a, T> Deref for MutexGuard<'a, T> {
    type Target = T;
    fn deref(&self) -> &T {
        unsafe {
            &*(self.0).1.get()
        }
    }
}

impl<'a, T> DerefMut for MutexGuard<'a, T> {
    fn deref_mut(&mut self) -> &mut T {
        unsafe {
            &mut *(self.0).1.get()
        }
    }
}


/*
pub struct MpmcQueue<T> where T: Send {
    lock: Mutex<()>,
    head: Option<Box<T>>,
    tail: Option<Box<T>>
}

//unsafe impl<T> Send for MpmcQueue<T> where T: Send { }
//unsafe impl<T> Sync for MpmcQueue<T> where T: Send { }

impl<T> MpmcQueue<T> where T: Send {
    const fn new() -> MpmcQueue<T> {
        MpmcQueue {
            lock: Mutex::new(),
            head: None,
            tail: None
        }
    }

    pub fn push(&self, x: T) {
        unimplemented!();
    }

    pub fn pop(&self) -> T {
        unimplemented!();
    }

    pub fn try_pop(&self) -> Option<T> {
        unimplemented!();
    }
}
*/

#[cfg(all(test, not(target_env="")))]
mod test {
    use super::*;
    use std::sync::atomic::{Ordering, AtomicIsize};
    use std::thread::spawn;

    static M: Mutex<()> = Mutex::new(());
    static I: AtomicIsize = AtomicIsize::new(0);

    static M2: Mutex<u8> = Mutex::new(0);

    #[test]
    fn is_mutually_exclusive() {
        // Perform some operations with relaxed ordering, depending entirely on
        // the fences implicit in mutex ordering. This is far from a comprehensive
        // test, but good for a quick check.
        spawn(|| {
            assert_eq!(I.load(Ordering::Relaxed), 0);
            {
                let _l = M.lock();
                I.fetch_add(1, Ordering::Relaxed);
                // Wait for other thread to try lock and verify it fails.
                while I.load(Ordering::Relaxed) == 1 {}
                assert_eq!(I.load(Ordering::Relaxed), 2);
            } // drop lock
            I.fetch_add(1, Ordering::Relaxed);
        });

        // Wait for lock
        while I.load(Ordering::Relaxed) == 0 {}
        // Ensure lock fails
        assert_eq!(M.try_lock().unwrap_err(), LockError::Locked);
        // Carry on
        I.fetch_add(1, Ordering::Relaxed);
        // Wait for unlock
        while I.load(Ordering::Relaxed) == 2 {}
        // Now we should be able to take the lock
        assert!(M.try_lock().is_ok());
    }

    #[test]
    fn will_poison_on_panic() {
        let h = spawn(|| {
            // Need to bind the guard so it doesn't immediately fall out of scope
            let mut l = M2.lock();
            *l = 1;
            panic!("Intentional panic");
        });

        // Wait for thread to take lock and panic
        assert!(h.join().is_err());

        assert_eq!(M2.try_lock().unwrap_err(), LockError::Poisoned);
    }
}
