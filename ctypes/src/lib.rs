#![no_std]
#![allow(non_camel_case_types)]

#[cfg(target_arch="x86_64")]
mod arch {
    pub type c_char = u8;
    pub type c_short = i16;
    pub type c_int = i32;
    pub type c_long = i64;
    pub type c_ulong = u64;
    pub type c_longlong = i64;
    pub type c_ulonglong = u64;
}

#[cfg(target_os="linux")]
mod os {
    /// x86_32-specific Linux types
    #[cfg(target_arch="i686")]
    mod arch {
        pub type off_t = super::super::c_longlong;
    }

    /// x86_64-specific Linux types
    #[cfg(target_arch="x86_64")]
    mod arch {
        pub type off_t = super::super::c_longlong;
    }

    pub type size_t = usize;
    pub type ssize_t = isize;

    pub use self::arch::*;
}

pub use arch::*;
pub use os::*;
