//! Abstract allocator interface.
//!
//! This contains an interface to allocators which does not contain a dependency on
//! actual allocator implementation. Splitting it in this fashion allows the dependencies
//! of `rkos_alloc_imp` to depend on allocators without introducing a circular dependency
//! on the allocator.
//!
//! Before the global allocator may be used, it must be initialized through the
//! interface in `rkos_alloc_imp`.
#![no_std]

pub trait Allocator {
    /// Allocate `size` bytes with minimum alignment `align` in bytes.
    ///
    /// Return a pointer to the allocated memory.
    fn allocate(&self, size: usize, align: usize) -> *mut u8;
    /// Free an allocation of `old_size` bytes with alignment `align` in bytes.
    fn free(&self, ptr: *mut u8, old_size: usize, align: usize);
    /// Resize an allocation of `old_size` bytes to be `size` bytes in size, with alignment
    /// `align`.
    ///
    /// Return a pointer to the allocation, which may or may not have the same value
    /// as the input pointer. Bytes from allocation start to the minimum of the old and
    /// new sizes are preserved.
    fn reallocate(&self, ptr: *mut u8, old_size: usize, size: usize, align: usize) -> *mut u8;
    /// Like `reallocate`, but never move the allocation.
    ///
    /// Returns the new size of the allocation, which may not be the requested size.
    fn reallocate_inplace(&self, ptr: *mut u8, old_size: usize, size: usize, align: usize) -> usize;
    /// Return the minimum usable size of an allocation with given size and alignment.
    fn usable_size(&self, size: usize, align: usize) -> usize;
}

extern {
    fn __rust_allocate(size: usize, align: usize) -> *mut u8;
    fn __rust_deallocate(ptr: *mut u8, old_size: usize, align: usize);
    fn __rust_reallocate(ptr: *mut u8, old_size: usize, size: usize,
                         align: usize) -> *mut u8;
    fn __rust_reallocate_inplace(ptr: *mut u8, old_size: usize,
                                 size: usize, align: usize) -> usize;
    fn __rust_usable_size(size: usize, align: usize) -> usize;
}

/// A way to refer to the global heap where a specific (possibly subheap) is required.
pub struct GlobalAllocator;

impl Allocator for GlobalAllocator {
    fn allocate(&self, size: usize, align: usize) -> *mut u8 {
        unsafe {
            __rust_allocate(size, align)
        }
    }

    fn free(&self, ptr: *mut u8, old_size: usize, align: usize) {
        unsafe {
            __rust_deallocate(ptr, old_size, align)
        }
    }

    fn reallocate(&self, ptr: *mut u8, old_size: usize, size: usize, align: usize) -> *mut u8 {
        unsafe {
            __rust_reallocate(ptr, old_size, size, align)
        }
    }

    fn reallocate_inplace(&self, ptr: *mut u8, old_size: usize, size: usize, align: usize) -> usize {
        unsafe {
            __rust_reallocate_inplace(ptr, old_size, size, align)
        }
    }

    fn usable_size(&self, size: usize, align: usize) -> usize {
        unsafe {
            __rust_usable_size(size, align)
        }
    }
}
