use std::env;

fn main() {
    let target = env::var("TARGET").expect("Cargo TARGET not defined?!");
    let _profile = env::var("PROFILE").expect("Cargo PROFILE not defined?!");

    if !target_is_supported(&target) {
        panic!("Target `{}` is not supported.", target);
    }

    // debug_backend is required
    println!("cargo:rustc-cfg=debug_backend=\"stderr\"");
}

fn target_is_supported(target: &str) -> bool {
    ["x86_64-unknown-linux",
     "x86_64-pc-none"].contains(&target)
}
