# Building

Run `make`. There are some variables which can be set to control the build
process:

 * `DEBUG_MODE`: default `off`. If enabled, inhibits most compiler
   optimizations. Debug symbols are generated regardless, but tend to be
   difficult to make sense of when optimizations are turned on. However, the
   memory (stack) requirements tend to increase dramatically.

 * `DEBUG_BACKEND`: select the tool to use when invoking the `debug` target and
   using debug macros in source.
   * `none`: default. Debug macros do nothing, run under Qemu with a GDB stub
     available.
   * `bochs`: run under Bochs with GDB. debug macros print to the Bochs console
     and can break into the debugger, using the "port e9 hack". The emulator
     will wait for a GDB connection at startup.

Useful targets:

 * `all`: build a bootable disk image, `rkos.img`
 * `doc`: build HTML API documentation in the `doc` directory.
 * `debug`: run under the debugger specified by `DEBUG_BACKEND`

## Boot process

Firmware loads the boot code (`boot.S`) from the MBR, which by BIOS convention
loads the MBR (first sector of the booted disk) to 0x7C00 in real mode. The MBR
code loads the next 31 sectors from the disk, the stage2 boot code.

Stage2 boot code loads the system image (`rkos.elf`) from the second disk
partition, which is configured by `packelf.py` to have a declared length
appropriate to the size of the system image. This image is loaded into physical
memory at 0x200000, and is never moved after that.

A physical memory map is acquired and placed in memory at 0x5004, with the word
at 0x5000 indicating how large the memory map is. Stage2 then jumps to `kmain`.

The first thing Rust code does is set up the allocator, using the memory map
acquired by stage2.

## Notes

Virtio spec: http://docs.oasis-open.org/virtio/virtio/v1.0/virtio-v1.0.html

DWARF debuginfo specs: http://dwarfstd.org/Download.php
eh\_frame specs (similar to DWARF): http://refspecs.linuxfoundation.org/LSB_3.0.0/LSB-Core-generic/LSB-Core-generic/ehframechpt.html
