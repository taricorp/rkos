# Disable default suffix rules
.SUFFIXES:

## Tool locations (overridable)
# Maybe cargo -j 1 --verbose for debugging
CARGO ?= cargo

# Read config if exists
-include .config
# Set defaults
TARGET ?= x86_64-unknown-linux
PROFILE ?= release
DEBUG_BACKEND ?= dummy
# Write config
_:=$(shell mkdir -p target && \
           echo "\# RKOS build configuration generated" $(shell date) > .config && \
           echo TARGET ?= $(TARGET) >> .config && \
           echo PROFILE ?= $(PROFILE) >> .config && \
           echo DEBUG_BACKEND ?= $(DEBUG_BACKEND) >> .config)
ifneq ('$(_)','')
$(error $(_))
endif

## == CARGO BITS ==
# Pass to scripts/rustc to force sysroot when cross-compiling to
# a custom target
SYSROOT := $(shell pwd)/target/$(TARGET)/$(PROFILE)/sysroot
RUSTFLAGS := --sysroot=$(SYSROOT)

## Short version of cargo's target directories
TARGET_DIR := target/$(TARGET)/$(PROFILE)
_:=$(shell mkdir -p $(TARGET_DIR))

## Select cargo profile according to build profile
ifeq ($(PROFILE),release)
CARGO_REL := --release
else
CARGO_REL :=
endif

## Shorthand for building with cargo
CARGO_COMMAND = RUSTFLAGS=$(RUSTFLAGS) $(CARGO) $(1) --target $(TARGET) $(CARGO_REL)
CARGO_BUILD := $(call CARGO_COMMAND,build)
CARGO_RUSTDOC := $(call CARGO_COMMAND,rustdoc)

## == IMAGE CONFIGURATION ==
ifeq ($(TARGET),x86_64-pc-none)
# Freestanding PC
KIMAGE_NAME := rkos.img
else ifeq ($(TARGET),x86_64-unknown-linux)
# Linux-hosted
KIMAGE_NAME := rkos
else
$(error Target "$(TARGET)" is unknown)
endif


.PHONY: all
all: $(TARGET_DIR)/$(KIMAGE_NAME)

# Needs cargo-graph; `cargo install cargo-graph`
.PHONY: depgraph.png
depgraph.png:
	cargo graph --optional-line-style=dashed | dot -Tpng > depgraph.png

.PHONY: $(TARGET_DIR)/librkos.a
$(TARGET_DIR)/librkos.a: sysroot
	$(CARGO_BUILD)

.PHONY: sysroot
sysroot: $(SYSROOT)/lib/rustlib/$(TARGET)/lib/libcore.rlib

$(SYSROOT)/lib/rustlib/$(TARGET)/lib/libcore.rlib: dep/rust/src/libcore/$(TARGET_DIR)/libcore.rlib
	mkdir -p $(shell dirname $@)
	cp $^ $@

.PHONY: dep/rust/src/libcore/$(TARGET_DIR)/libcore.rlib
dep/rust/src/libcore/$(TARGET_DIR)/libcore.rlib:
	$(CARGO_BUILD) --manifest-path dep/rust/src/libcore/Cargo.toml


## TODO clean up below this point later.

.PHONY: help
help:
	@echo 'Targets:'
	@echo '  all: default; build a system image'
	@echo '  clean: remove artifacts for all targets'
	@echo '  clean-all: remove all artifacts and config'
	@echo '  doc: build HTML api documentation to 'doc' directory'
	@echo
	@echo 'Options: stored across invocations of 'make''
	@echo '  TARGET: select the host platform (see PLATFORMS)'
	@echo ' PROFILE: 'release' for full optimizations, otherwise do not optimize.'
	@echo
	@echo 'Platforms:'
	@echo '  x86_64-pc-none: "bare-metal" 64-bit PC (including most hypervisors)'
	@echo '  x86_64-unknown-linux: static Linux program'

# compiler-rt cannot (currently) build with GCC 5.2. Use clang.
# (error: conflicting types for built-in function '__multc3')
$(TARGET_DIR)/libcompiler_rt.a:
	make -C dep/rust/src/compiler-rt \
		CC=clang \
		TargetTriple=x86_64-unknown-unknown \
		triple-builtins
	if [ dep/rust/src/compiler-rt/triple/builtins/libcompiler_rt.a -nt $@ ]; \
	then cp dep/rust/src/compiler-rt/triple/builtins/libcompiler_rt.a $@; \
	fi

.PHONY: $(TARGET_DIR)/libopenlibm.a
$(TARGET_DIR)/libopenlibm.a:
	make -C dep/openlibm libopenlibm.a
	if [ dep/openlibm/libopenlibm.a -nt $@ ]; then cp dep/openlibm/libopenlibm.a $@; fi

## jemalloc
# ./configure --disable-prof-libgcc --disable-prof-gcc --disable-munmap --disable-fill
# --disable-valgrind --enable-xmalloc --disable-tls --host=x86_64-unknown-none

#librkos $(TARGET_DIR)/librkos.a: $(addprefix $(TARGET_DIR)/,libcore.rlib librlibc.rlib libopenlibm.a libcompiler-rt.a libtlsf.a)
#	rustc --target $(TARGET) --emit link,dep-info \
#		--crate-type staticlib --crate-name rkos \
#		--out-dir $(TARGET_DIR) \
#		--emit dep-info,link \
#		$(RUSTC_OPT) \
#		-lstatic=openlibm -lstatic=tlsf -L$(TARGET_DIR) \
#		--sysroot . \
#		--cfg debug_backend=\"$(DEBUG_BACKEND)\" \
#		src/lib.rs

target/x86_64-pc-none.%/boot.elf: boot.S boot.ld
	gcc -static -nostdlib -Wl,-Tboot.ld -Wl,--build-id=none \
		-o $(TARGET_DIR)/boot.elf \
		boot.S

target/x86_64-pc-none.%/kernel.elf: $(TARGET_DIR)/librkos.a
	gcc -static -nostdlib -Wl,-Tx86_64-pc-none.ld \
		-o $(TARGET_DIR)/kernel.elf \
		$(TARGET_DIR)/librkos.a

target/x86_64-pc-none.%/$(KIMAGE_NAME): $(addprefix $(TARGET_DIR)/,boot.elf kernel.elf)
	# Make flat binary image from ELF
	objcopy -O binary $(TARGET_DIR)/kernel.elf $(TARGET_DIR)/kernel.bin
	# Same for boot
	objcopy -O binary $(TARGET_DIR)/boot.elf $(TARGET_DIR)/boot.bin
	# Pad to size of boot area and begin writing image
	dd if=$(TARGET_DIR)/boot.bin of=$(TARGET_DIR)/rkos.img count=32
	# Set loader parameters: entry point address and size of binary image (in sectors)
	./packelf.py $(TARGET_DIR)/rkos.img $(TARGET_DIR)/kernel.elf $(TARGET_DIR)/kernel.bin
	# Append binary image, pad to an integer number of sectors
	dd if=$(TARGET_DIR)/kernel.bin of=$(TARGET_DIR)/rkos.img seek=32 conv=sync

ifeq ($(TARGET),x86_64-unknown-linux)
$(TARGET_DIR)/rt0.o: src/platform/linux-x86_64/rt0.S
	gcc -c -o $@ $^

$(TARGET_DIR)/$(KIMAGE_NAME): $(addprefix $(TARGET_DIR)/,rt0.o librkos.a libopenlibm.a libcompiler_rt.a)
	gcc -static -nostdlib \
		-Wl,-Tx86_64-unknown-linux.ld \
		-o $(TARGET_DIR)/$(KIMAGE_NAME) \
		$^
endif

.PHONY: clean clean-all
clean:
	$(CARGO) clean
	$(CARGO) clean --manifest-path dep/rust/src/libcore/Cargo.toml
	rm -f depgraph.png

clean-all:
	rm -f .config

doc: sysroot
	find . ! -path './dep/*' -name Cargo.toml -type f | (\
		while read p; \
		do \
			PKG=$$(grep 'name \?= \?' $$p | cut -d= -f2 | cut -d\" -f2); \
			$(CARGO_RUSTDOC) -p $$PKG -L$(SYSROOT)/lib/rustlib/$(TARGET)/lib; \
		done )


## This stuff is kind of wonky
.PHONY: debug debug-bochs debug-qemu
ifeq ($(DEBUG_BACKEND), bochs)
debug: debug-bochs
else
debug: debug-qemu
endif

debug-bochs: debug/rkos.syms debug/rkos.img
	bochs -f debug/bochsrc.bxrc

debug/rkos.img: rkos.img
	bximage -q -mode=create -hd=32M debug/rkos.img
	dd if=rkos.img of=debug/rkos.img conv=notrunc

debug/rkos.syms: kernel.elf boot.elf
	nm --defined-only kernel.elf | awk '{ print $$1, $$3; }' > debug/rkos.syms
	nm --defined-only boot.elf | awk '{ print $$1, $$3; }' >> debug/rkos.syms

debug-qemu: $(TARGET_DIR)/rkos.img
	qemu-system-x86_64 $(TARGET_DIR)/rkos.img -s \
		-d guest_errors,cpu_reset,int \
		-serial stdio
		#-curses

gdb: $(TARGET_DIR)/kernel.elf
	$(shell which rust-gdb >/dev/null && echo -n rust-)gdb $(TARGET_DIR)/kernel.elf
