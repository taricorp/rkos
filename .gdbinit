# For early boot before switch to long mode.
#set architecture i8086
#tb *0x7c00

b rust_begin_unwind
#target remote localhost:1234

# XXX hack
b rkos::debug::wait_for_attach_inner
commands
    set $eax=1
    continue
end
