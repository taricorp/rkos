//! Legacy (8259) programmable interrupt controller.
//!
//! The PC has two PICs, each with command and data ports. The master is at I/O address 0x20
//! (command) and 0x21 (data), while the slave is at 0xA0 and 0xA1.

use arch::x86::io::{inb, outb};

const MASTER_CMD: u16 = 0x20;
const MASTER_DAT: u16 = 0x21;
const SLAVE_CMD: u16 = 0xA0;
const SLAVE_DAT: u16 = 0xA1;

// command register, data register, vector offset, ICW3 value
static PIC_INIT: [(u16, u16, u8, u8); 2] = [
    // slave PIC exists at IRQ2
    (MASTER_CMD, MASTER_DAT, 32u8, 4),
    // cascade identity is 2
    (SLAVE_CMD, SLAVE_DAT, 40u8, 2)
];

fn disable() {
    // Mask all IRQs both PICs
    outb(MASTER_DAT, 0xff);
    outb(SLAVE_DAT, 0xff);

    // Remap PIC IRQs to start at 32, protecting against spurious interrupts (even when masked..)
    // To do so we need to run through the whole PIC initialization sequence. Refer to the Intel
    // 8259A datasheet for details.
    for (cmd_reg, dat_reg, ofs, icw3) in PIC_INIT {
        // ICW1: begin init, with ICW4
        outb(cmd_reg, 0x11);
        // ICW2: vector offset
        outb(dat_reg, ofs);
        // ICW3: slave configuration
        outb(dat_reg, icw3);
        // ICW4: in 8086 mode
        outb(dat_reg, 1);
    }
}
