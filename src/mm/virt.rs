//! Virtual memory management.
//! 
//! Page table fields have cryptic names. They are as follow:
//! 
//!  * D - set to 1 on write
//!  * PS - combine with CR4.PAE and EFER.LMA to determine page size
//!  * G - if 1, TLB for the page is not invalidated on load to CR3 or task switch
//!  * AVL - free for software use
//!  * PAT - page attribute table something-or-other
//!  * NX - prohibit execution from page if 1 and EFER.NXE=1
//!  * A - set to 1 on access
//!  * PCD - non-cachable if 1
//!  * PWT - writeback caching if 0, otherwise write-through
//!  * U/S - not readable by CPL3 if 0
//!  * R/W - read-only if 0
//!  * P - next level exists in physical memory (#PF on access if 0)
//!
//! The page table structure (on x86_64) is PML4 -> PDPT -> PDT -> PT. Each
//! level is an array of 512 entries at a 4k-aligned physical address. PDT
//! may be omitted in which case the corresponding PDPT entry refers to a 1GB
//! physical page, or PT may be omitted in which case the corresponding PDT
//! entry refers to a 2MB physical page.

use core;
use core::mem::transmute;
use core::slice;
use super::{FixedSizeArena, PhysPtr};
use super::meta::KERNEL_CODE_END;
use super::PtrAlign;

/// Declare a struct describing a page table entry.
///
/// These entries are assumed to have A, PCD, PWT, U/S and R/W bits.
macro_rules! pt_decl {
    ($name:ident) => (
        pub struct $name(u64);
        impl AsRef<u64> for $name {
            fn as_ref(&self) -> &u64 {
                &self.0
            }
        }
        impl AsMut<u64> for $name {
            fn as_mut(&mut self) -> &mut u64 {
                &mut self.0
            }
        }
        impl HasPCD for $name {}
        impl HasUS for $name {}
        impl HasRW for $name {}
        impl HasA for $name {}
        impl HasP for $name {}
    )
}

#[inline]
fn write_bits(x: &mut u64, nbits: u8, shift: u8, value: u64) {
    assert!(nbits <= 64);
    if nbits == 64 {
        *x = value;
        return;
    }

    let mut mask = (1 << nbits) - 1;
    // value must not have any bits set beyond the low `nbits` bits.
    assert!(value & !mask == 0);

    mask <<= shift;
    *x &= !mask;
    *x |= value << shift;
}

#[inline]
fn read_bits(x: u64, nbits: u8, shift: u8) -> u64 {
    assert!(nbits <= 64);
    if nbits == 64 {
        return x
    }

    let mask = (1 << nbits) - 1;
    (x >> shift) & mask
}

/// PML4 entry
pt_decl!(PML4E);
/// PDPT entry
pt_decl!(PDPE);
impl PDPE {
    pub fn is_large_page(&self) -> bool {
        (self.0 & 0x80) != 0
    }
}
/// PDT entry
pt_decl!(PDE);
impl PDE {
    pub fn is_large_page(&self) -> bool {
        (self.0 & 0x80) != 0
    }
}
/// PT entry
pt_decl!(PTE);

/// Top-level page table, corresponding to bits 39 through 47 of a virtual address
pub type PML4 = [PML4E; 512];
/// Third-level page table, corresponding to bits 30 through 38 of a virtual address
pub type PDP = [PDPE; 512];
/// Second-level page table, bits 21 through 29 of a virtual address.
pub type PD = [PDE; 512];
/// Bottom-level page table, bits 12 through 20 of a virtual address.
pub type PT = [PTE; 512];

/// Encodings for PCD and PWT bits.
pub enum CachePolicy {
    /// May not be cached.
    NonCacheable = 2,
    /// Write-back cached.
    WriteBack = 0,
    /// Write-through cached.
    WriteThrough = 1
}

impl From<u64> for CachePolicy {
    fn from(x: u64) -> CachePolicy {
        match x {
            0 => CachePolicy::WriteBack,
            1 => CachePolicy::WriteThrough,
            2 | 3 => CachePolicy::NonCacheable,
            x => panic!("CachePolicy must be a two-bit value only (got {})", x)
        }
    }
}

/// A page table level with PCD and PWT bits.
trait HasPCD: AsMut<u64> {
    /// Set the PCD and PWT bits.
    fn set_cache_policy(&mut self, policy: CachePolicy) {
        write_bits(self.as_mut(), 2, 3, policy as u64);
    }
}

/// A page table level with a U/S bit.
trait HasUS: AsMut<u64> {
    /// Set the U/S bit.
    fn set_supervisor_only(&mut self, s_only: bool) {
        write_bits(self.as_mut(), 2, 1, if s_only {
            0
        } else {
            1
        });
    }
}

/// A page table level with a R/W bit.
trait HasRW: AsMut<u64> {
    /// Set the R/W bit.
    fn set_writable(&mut self, writable: bool) {
        write_bits(self.as_mut(), 1, 1, if writable {
            1
        } else {
            0
        });
    }
}

/// A page table level with a A bit.
trait HasA: AsMut<u64> {
    fn set_accessed(&mut self, _: bool) {
        unimplemented!()
    }

    fn get_accessed(&self) -> bool {
        unimplemented!()
    }
}

trait HasP: AsMut<u64> + AsRef<u64> {
    fn set_present(&mut self, present: bool) {
        write_bits(self.as_mut(), 1, 0, if present { 1 } else { 0 });
    }

    fn get_present(&self) -> bool {
        read_bits(*self.as_ref(), 1, 0) == 1
    }
}

trait HasAddr<T>: AsMut<u64> + AsRef<u64> {
    #[inline]
    fn get_phys_addr(&self) -> PhysPtr<T> {
        // Physical address is always bits 12 through 51
        let pa = read_bits(*self.as_ref(), 40, 12) << 12;
        PhysPtr(pa as *mut T)
    }

    fn set_phys_addr(&mut self, pa: PhysPtr<T>) {
        let pa: u64 = pa.0 as u64;
        // Low 12 bits are assumed zero by hardware (and used for flags), high 12 bits are
        // ignored by hardware (and bit 63 is the NX flag).
        assert_aligned!(pa as usize, 4096);
        assert_eq!(pa & !((1 << 52) - 1), 0);
        *self.as_mut() |= pa;
    }
}
impl HasAddr<PDP> for PML4E {}
// PDPE may refer to a PD or a single 1G page.
impl HasAddr<PD> for PDPE {}
// TODO HasAddr API should work for a DirEntry here. get_phys_addr permits taking the wrong
// type.
impl HasAddr<()> for PDPE {
    fn set_phys_addr(&mut self, pa: PhysPtr<()>) {
        // TODO messy code duplication
        // TODO HasAddr API should work for a DirEntry here.
        let pa: u64 = pa.0 as u64;
        assert_aligned!(pa as usize, 4096);
        assert_eq!(pa & !((1 << 52) - 1), 0);
        // Bit 7 indicates a large page
        *self.as_mut() |= pa | 0x80;
    }
}
// PDE may refer to a PT of 4k pages or a single 2M page.
impl HasAddr<PT> for PDE {}
// TODO HasAddr API should work for a DirEntry here. get_phys_addr permits taking the wrong
// type.
impl HasAddr<()> for PDE {
    fn set_phys_addr(&mut self, pa: PhysPtr<()>) {
        // TODO messy code duplication
        let pa = pa.0 as u64;
        assert_aligned!(pa as usize, 4096);
        assert_eq!(pa & !((1 << 52) - 1), 0);
        // Bit 7 indicates a large page
        *self.as_mut() |= pa | 0x80;
    }
}
impl HasAddr<()> for PTE {}

pub trait PTForVA<T> {
    /// Get a pointer to the PTE corresponding to a given VA.
    fn entry_for_va<U>(&mut self, ptr: *mut U) -> &mut T;
}

impl PTForVA<PML4E> for PML4 {
    fn entry_for_va<U>(&mut self, ptr: *mut U) -> &mut PML4E {
        let ofs = read_bits(ptr as u64, 9, 39) as usize;
        &mut self[ofs]
    }
}

impl PTForVA<PDPE> for PDP {
    fn entry_for_va<U>(&mut self, ptr: *mut U) -> &mut PDPE {
        let ofs = read_bits(ptr as u64, 9, 30) as usize;
        &mut self[ofs]
    }
}

impl PTForVA<PDE> for PD {
    fn entry_for_va<U>(&mut self, ptr: *mut U) -> &mut PDE {
        let ofs = read_bits(ptr as u64, 9, 21) as usize;
        &mut self[ofs]
    }
}

impl PTForVA<PTE> for PT {
    fn entry_for_va<U>(&mut self, ptr: *mut U) -> &mut PTE {
        let ofs = read_bits(ptr as u64, 9, 12) as usize;
        &mut self[ofs]
    }
}

impl PML4E {
    pub fn new(pdp: Option<PhysPtr<PDP>>) -> PML4E {
        let mut out = PML4E(0);
        if let Some(addr) = pdp {
            out.set_phys_addr(addr);
            out.set_present(true);
        }
        out
    }
}

/// Contents of the memory region described by a `PDPE` or `PDE`.
pub enum DirEntry<T> {
    /// Physical page.
    Page(PhysPtr<()>),
    /// Next-level page directory.
    Directory(PhysPtr<T>)
}

impl PDPE {
    pub fn new(contents: Option<DirEntry<PD>>) -> PDPE {
        let mut out = PDPE(0);
        match contents {
            Some(DirEntry::Page(addr)) => {
                out.set_phys_addr(addr);
                out.set_present(true);
                // TODO method for this?
                // 1G page is bit 7
                out.0 |= 0x80;
            },
            Some(DirEntry::Directory(addr)) => {
                out.set_phys_addr(addr);
                out.set_present(true);
            },
            // Not present
            None => {}
        }
        out
    }
}

impl PDE {
    pub fn new(contents: Option<DirEntry<PT>>) -> PDE {
        let mut out = PDE(0);
        match contents {
            Some(DirEntry::Page(addr)) => {
                out.set_phys_addr(addr);
                out.set_present(true);
                // TODO method for this?
                // 2M page is bit 7
                out.0 |= 0x80;
            },
            Some(DirEntry::Directory(addr)) => {
                out.set_phys_addr(addr);
                out.set_present(true);
            },
            None => {}
        }
        out
    }
}

impl PTE {
    pub fn new(page: Option<PhysPtr<()>>) -> PTE {
        let mut out = PTE(0);

        if let Some(addr) = page {
            out.set_phys_addr(addr);
            out.set_present(true);
        }
        out
    }
}

/// Set the page table root (PML4).
pub unsafe fn set_pt_root(addr: PhysPtr<PML4>, cacheable: CachePolicy) {
    let mut addr = addr.0 as u64;
    // Reserved bits must write zero, ignore PCD, PWT for now
    assert_eq!(addr & 0xFFF0000000000FFF, 0);
    addr |= (cacheable as u64) << 3;
    asm!("mov $0, %cr3"
         : /* No outputs */
         : "r"(addr)
         : /* No clobbers */
         : "volatile");
    // Hardware invalidates all TLB entries on store to CR3
}

/// Get the current page table root.
pub unsafe fn get_pt_root() -> (PhysPtr<PML4>, CachePolicy) {
    let cr3: u64;
    asm!("mov %cr3, $0"
         : "=r"(cr3)
         : /* No inputs */
         : /* No clobbers */
         : "volatile");
    (PhysPtr((cr3 & !0xFFF) as *mut _),
     CachePolicy::from((cr3 >> 3) & 3))
}

/// Invalid the TBL entry (if any) associated with the given virtual address.
pub fn invalidate_tlb<T>(addr: *const T) {
    unsafe {
        asm!("invlpg $0"
             : /* No outputs */
             : "m"(addr) // XXX not sure if m is the right constraint here (r?)
             : /* No clobbers */
             : "volatile");
    }
}

/// Invalidate all TLB entries.
pub fn invalidate_tlb_all() {
    // Setting CR3 implicitly invalidates all TLB.
    unsafe {
        let (p4, c) = get_pt_root();
        set_pt_root(p4, c);
    }
}

/// Get the physical address of a given virtual address, if any.
pub fn v2p<T>(addr: *mut T) -> Option<PhysPtr<T>> {
    let pml4 = unsafe { &mut *get_pt_root().0.va() };
    let pml4e = pml4.entry_for_va(addr);
    if !pml4e.get_present() {
        return None;
    }

    let pdp = unsafe { &mut *pml4e.get_phys_addr().va() };
    let pdpe = pdp.entry_for_va(addr);
    if !pdpe.get_present() {
        return None;
    } else if pdpe.is_large_page() {
        let ofs = (addr as isize) & (1 << 30);
        unsafe {
            let pa = HasAddr::<()>::get_phys_addr(pdpe).offset(ofs);
            return Some(PhysPtr::id_mapped(pa.unwrap() as *mut T));
        }
    }
    unimplemented!()
}

/// Shortcut for allocating page tables, rather than putting transmutes everywhere.
///
/// Not public by design.
trait PTAllocator<T> {
    fn allocate(&self) -> &mut T;
}

macro_rules! impl_ptallocator {
    ($t:ty) => (
        impl PTAllocator<$t> for FixedSizeArena<[u8; 4096]> {
            fn allocate(&self) -> &mut $t {
                unsafe {
                    transmute::<&mut [u8; 4096], &mut $t>(
                        self.alloc([0; 4096]).expect(
                            concat!("PTAllocator OOM for ", stringify!($t))
                        )
                    )
                }
            }
        }
    )
}

impl_ptallocator!(PML4);
impl_ptallocator!(PDP);
impl_ptallocator!(PD);
impl_ptallocator!(PT);

/// Unmap non-reserved boot memory, from address zero through `limit` exclusive.
pub unsafe fn unmap_non_reserved(pml4: &mut PML4,
                                 arena: &FixedSizeArena<[u8; 4096]>,
                                 limit: usize) {
    debug!("VM bootstrap: map reserved 0-{:#x}", limit);
    // Implementation assumes code fits in PML4[0][0] (PDP)
    assert!(limit < (1 << 30));

    // PDP for the first 512 gigabytes
    // Set PML4 to point here later since the complete VA mapping for code
    // must be installed before we can switch to it.
    let pdp: &mut PDP = arena.allocate();

    // PDT for the first gigabyte
    let pd: &mut PD = arena.allocate();
    pdp[0] = PDPE::new(Some(DirEntry::Directory(PhysPtr::id_mapped(pd))));

    const TWO_MEG: usize = 2 * (1 << 20);
    const FOUR_K: usize = 4 * (1 << 10);
    let mut addr = 0usize;

    // Consume any available 2MB chunks, including a possible tail chunk that
    // is less than one page (4k) smaller than a 2MB large page.
    // TODO not entirely satisfied this condition is correct.
    while addr + TWO_MEG - FOUR_K <= limit {
        pd[addr / TWO_MEG] = PDE::new(Some(DirEntry::Page(
            PhysPtr::id_mapped(addr as *mut ())
        )));
        addr += TWO_MEG;
    }

    // And 4k pages to fill out the rest.
    if addr < limit {
        let pt: &mut PT = arena.allocate();
        pd[addr / TWO_MEG] = PDE::new(Some(DirEntry::Directory(
            PhysPtr::id_mapped(pt)
        )));

        let mut ofs = 0usize;
        while addr + ofs < limit {
            pt[ofs / FOUR_K] = PTE::new(Some(
                PhysPtr::id_mapped((addr + ofs) as *mut ())
            ));
            ofs += FOUR_K;
        }
    }

    // Now we can hook the things we just created into the live PT root.
    pml4[0] = PML4E::new(Some(PhysPtr::id_mapped(pdp)));
}

/// Bootstrap the identity mapping of all of physical memory.
///
/// Assumes that memory returned by the given arena is in an identity-mapped region.
pub unsafe fn bootstrap_idmap(pml4: &mut PML4,
                              arena: &FixedSizeArena<[u8; 4096]>,
                              mut phys_size: usize) {
    debug!("VM bootstrap: create idmap for {:#x} bytes", phys_size);
    // Too much physical memory
    if phys_size > (super::STACK_START - super::IDMAP_START) {
        panic!("Too much physical memory");
    }
    let mut page_count = 0usize;

    while phys_size > 0 {
        // Offset 256 since we only operate on the high half of virtual memory
        // TODO use entry_for_va?
        let pml4e: &mut PML4E = &mut pml4[256 + (page_count / 512)];

        // Allocate or get PDPE
        let pdpe: &mut PDPE = if !pml4e.get_present() {
            let new_pdp: &mut PDP = arena.allocate();
            pml4e.set_phys_addr(PhysPtr::id_mapped(new_pdp));
            pml4e.set_present(true);
            &mut new_pdp[0]
        } else {
            &mut (*pml4e.get_phys_addr().va())[page_count % 512]
        };

        // Fill in PDPE
        let pa = (1 << 30) * page_count;
        let va = pa + super::IDMAP_START;
        *pdpe = PDPE::new(Some(DirEntry::Page(PhysPtr::id_mapped(pa as *mut ()))));
        invalidate_tlb(va as *const ());

        // One more 1G page claimed
        page_count += 1;
        // Update remaining memory size to claim
        if phys_size < (1 << 30) {
            phys_size = 0;
        } else {
            phys_size -= 1 << 30;
        }
    }
}

/// Enumerates available physical memory regions, largest to smallest.
///
/// An "available" region satisfies all of the following conditions:
///
///  1. The BIOS-reported memory map marks it as "available"
///  2. It is aligned at a 4k, 2M or 1G boundary.
///  3. The size of the region is equal to its alignment.
///  4. It does not overlap with boot-time reserved memory (0 through
///     KERNEL_CODE_END).
struct IterAlignedRegions<I> {
    state: IterAlignedRegionsState,
    start: I,
    cur: I,
    align: usize
}

#[derive(PartialEq, Debug)]
enum IterAlignedRegionsState {
    Initial,
    Consuming(usize, usize)
}

const ALIGNMENTS: [usize; 3] = [1 << 30, 2 << 20, 4 << 10];

impl<'a, I> IterAlignedRegions<I>
        where I: Iterator<Item=&'a super::phys::RegionDescriptor> + Clone {
    pub fn new(iter: I) -> IterAlignedRegions<I> {
        IterAlignedRegions {
            state: IterAlignedRegionsState::Initial,
            start: iter.clone(),
            cur: iter,
            align: 0
        }
    }
}

impl<'a, I> Iterator for IterAlignedRegions<I> 
        where I: Iterator<Item=&'a super::phys::RegionDescriptor> + Clone {
    type Item = (PhysPtr<()>, usize);

    fn next(&mut self) -> Option<Self::Item> {
        use self::IterAlignedRegionsState::*;

        /// Return whether a region is usable at the given alignment and size.
        /// Tests the conditions described in docs for this struct.
        fn is_valid_region(start: usize, len: usize, alignment: usize) -> bool {
            (start & (alignment - 1)) == 0 &&
            len >= alignment &&
            start > (KERNEL_CODE_END as usize)
        }

        'regions: loop {
            // Get into a Consuming state
            if self.state == Initial {
                self.state = match self.cur.next() {
                    Some(r) if r.is_available() => Consuming(r.base as usize, r.length as usize),
                    // Take another region if not available
                    Some(_) => continue 'regions,
                    None => {
                        // Out of regions, so advance to next-smallest alignment
                        self.cur = self.start.clone();
                        self.align += 1;
                        if self.align >= ALIGNMENTS.len() {
                            return None;
                        } else {
                            // Tail recurse
                            continue 'regions;
                        }
                    }
                };
            }
            debug_assert!(self.state != Initial);

            if let Consuming(start, mut len) = self.state {
                // target alignment must be a power of two
                let target_alignment = ALIGNMENTS[self.align];
                // Align start address forward to target alignment
                let aligned = start.align_forward(target_alignment);

                if len < (aligned - start) {
                    len = 0;
                } else {
                    len -= aligned - start;
                }

                // Must not be a valid block in a preceding alignment.
                for &larger_alignment in ALIGNMENTS[..self.align].iter() {
                    if is_valid_region(aligned, len, larger_alignment) {
                        // Was consumed by a larger-alignment region. Skip the larger-alignment
                        // subregion.
                        assert!(len >= larger_alignment);
                        self.state = if len == larger_alignment {
                            Initial
                        } else {
                            Consuming(aligned + larger_alignment, len - larger_alignment)
                        };
                        continue 'regions;
                    }
                }

                // Whether this is a valid region or not, it's being consumed.
                self.state = if len <= target_alignment {
                    Initial
                } else {
                    Consuming(aligned + target_alignment, len - target_alignment)
                };

                if is_valid_region(aligned, len, target_alignment) {
                    return unsafe {
                        Some((PhysPtr::id_mapped(aligned as *mut ()), target_alignment))
                    };
                }
            }
        }
    }
}

// TODO this is a platform function
pub unsafe fn bootstrap_heap<'a, I>(arena: &mut FixedSizeArena<[u8; 4096]>,
                                    iter: I)
        where I: Iterator<Item=&'a super::phys::RegionDescriptor> + Clone {
    let mut va = super::HEAP_START;
    let pml4 = &mut *get_pt_root().0.va();

    // Yields regions suitable for page allocation from largest to smallest
    for (ptr, len) in IterAlignedRegions::new(iter) {
        debug!("bootstrap_heap got {}-byte region at {:p}, installing to {:#x}", len, ptr, va);
        let pml4e: &mut PML4E = pml4.entry_for_va(va as *mut ());
        if !pml4e.get_present() {
            let new_pdp: &mut PDP = arena.allocate();
            *pml4e = PML4E::new(Some(PhysPtr::id_mapped(new_pdp)));
        }

        // Walk the PT hierarchy and install a page for the current region.
        let pdp = &mut *pml4e.get_phys_addr().va();
        let pdpe = pdp.entry_for_va(va as *mut ());
        if len == (1 << 30) {
            // 1G page in PDP
            *pdpe = PDPE::new(Some(DirEntry::Page(ptr)));
        } else {
            if !pdpe.get_present() {
                let new_pd: &mut PD = arena.allocate();
                *pdpe = PDPE::new(Some(DirEntry::Directory(PhysPtr::id_mapped(new_pd))));
            }
            let pd: &mut PD = &mut *pdpe.get_phys_addr().va();
            let pde = pd.entry_for_va(va as *mut ());

            if len == (2 << 20) {
                // 2M page in PD
                *pde = PDE::new(Some(DirEntry::Page(ptr)));
            } else {
                // 4k page in PT
                if !pde.get_present() {
                    let new_pt: &mut PT = arena.allocate();
                    *pde = PDE::new(Some(DirEntry::Directory(PhysPtr::id_mapped(new_pt))));
                }
                let pt: &mut PT = &mut *pde.get_phys_addr().va();

                *pt.entry_for_va(va as *mut ()) = PTE::new(Some(ptr));
            }
        }

        // Advance VA past newly-installed page.
        va += len;
    }
}

/// Write a textual representation of the entire virtual memory space to output.
pub fn print_vm_map<W: ::core::fmt::Write>(w: &mut W) -> core::fmt::Result {
    let pml4: &PML4 = unsafe {
        &*get_pt_root().0.va()
    };

    for (i, pml4e) in pml4.iter().enumerate() {
        if pml4e.get_present() {
            let mut p4_va = (512 * (1 << 30)) * (i % 0x100);
            if i >= 0x100 {
                p4_va |= 0xFFFF800000000000;
            }

            try!(write!(w, "PML4[{:03x}] PA={:p}\n", i, pml4e.get_phys_addr().0));

            let pdp: &PDP = unsafe {
                &*pml4e.get_phys_addr().va()
            };
            try!(print_pdp_map(w, p4_va, pdp));
        }
    }

    Ok(())
}

/// Used by `print_vm_map`
fn print_pdp_map<W: ::core::fmt::Write>(w: &mut W, base_addr: usize, pdp: &PDP)
        -> core::fmt::Result {
    for (i, pdpe) in pdp.iter().enumerate() {
        let va = base_addr | i * (1 << 30);
        let pa = <PDPE as HasAddr<()>>::get_phys_addr(pdpe).0;

        if pdpe.get_present() {
            try!(write!(w, "  PDP[{:03x}] PA={:p}", i, pa));
            if pdpe.is_large_page() {
                try!(write!(w, " {:#018x}", va));
            }
            try!(write!(w, "\n"));
            
            if !pdpe.is_large_page() {
                try!(print_pd_map(w, va, unsafe {
                    &*pdpe.get_phys_addr().va()
                }));
            }
        }
    }

    Ok(())
}

/// Used by `print_pdp_map`
fn print_pd_map<W: ::core::fmt::Write>(w: &mut W, base_addr: usize, pd: &PD)
        -> core::fmt::Result {
    for (i, pde) in pd.iter().enumerate() {
        let va = base_addr | i * (2 << 20);
        let pa = <PDE as HasAddr<()>>::get_phys_addr(pde).0;

        if pde.get_present() {
            try!(write!(w, "    PDE[{:03x}] PA={:p}", i, pa));

            if pde.is_large_page() {
                try!(write!(w, " {:#018x}", va));
            }
            try!(write!(w, "\n"));

            if !pde.is_large_page() {
                try!(print_pt_map(w, va, unsafe {
                    &*pde.get_phys_addr().va()
                }));
            }
        }
    }

    Ok(())
}

/// Used by `print_pd_map`
fn print_pt_map<W: ::core::fmt::Write>(w: &mut W, base_addr: usize, pt: &PT)
        -> core::fmt::Result {
    for (i, pte) in pt.iter().enumerate() {
        let va = base_addr | i * (4 << 10);
        let pa = pte.get_phys_addr().0;

        if pte.get_present() {
            try!(write!(w, "      PTE[{:03x}] {:#018x} PA={:p}\n", i, va, pa));
        }
    }

    Ok(())
}
