//! Memory management.
//!
//! In general, all memory is directly mapped physical-to-virtual with no protection. All of
//! physical memory is part of a memory pool managed with TLSF. Thread stacks are the only true
//! users of virtual memory, mapped into the top of virtual memory. From `(top of memory) - (thread
//! stack size)` to `(top of physical memory)` is unmapped virtual memory.
//!
//! The advantage of this approach is that allocation of both physical and virtual memory need not
//! be tracked. Only physical memory allocation is tracked, and virtual memory allocation is
//! implicit according to the active thread.
//! 
//! # Implementation details
//!
//! There must always be some PTEs mapped into virtual memory. The bootstrap code puts the tables
//! for the first 512GB of memory at 0x1000 through 0x4FFF, identity-mapped so we have that
//! available during initialization.
//! 
//! Each thread needs its own page tables, which will be allocated on the heap. Thus we can't use
//! an otherwise-free chunk of memory (like the second megabyte), because it only works for one
//! thread (or at least doesn't scale to a lot of threads). Since we need to be able to get a
//! virtual address for any given physical address, we'll map all of memory into the top half of
//! the virtual address space.
//!
//! Virtual memory map:
//!  * 0 - 2M: currently unused
//!  * 2M - ???: kernel code (identity-mapped)
//!  * 1G - ???: heap (arbitrary mappings)
//!  * 0x8000.00000000 - 0xFFFF8000.00000000: canonical address hole (required by the architecture)
//!  * 0xFFFF8000.00000000 - ???: mirror of physical memory starting from 0
//!  * top - 512 GiB: unmapped (always guard page)
//!  * top 511 GB: thread stack
//! 
//! This implies the following limitations:
//!  * 128 TiB - 512 GiB maximum heap size
//!  * 511 GiB maximum stack size per thread (this could be relaxed to 512 GiB - 4 KiB)
//! 
//! The current implementation of the `virt::bootstrap_*` imposes artificial limits of
//! approximately half the theoretical maximum heap.

const HEAP_START: usize  = 1 << 30;
const IDMAP_START: usize = 0xFFFF800000000000;
const STACK_START: usize = 0xFFFFFDFFFFFFFFFF;

/// Assert that `ptr` is aligned to a `n`-byte boundary.
///
/// `n` must be a power of two.
macro_rules! assert_aligned {
    ($ptr:expr, $n:expr) => ({
        use ::mm::PtrAlign;
        assert!($ptr . is_aligned($n));
    })
}

pub mod phys;
pub mod virt;

use core::{intrinsics, mem, ptr};
use core::cell::UnsafeCell;
use core::mem::transmute;

trait PtrAlign: Sized + Eq {
    fn align_forward(&self, align: usize) -> Self;

    fn align_for_type<T>(&self) -> Self {
        self.align_forward(::core::mem::align_of::<T>())
    }

    fn is_aligned(&self, align: usize) -> bool {
        self.align_forward(align) == *self
    }

    fn is_aligned_for_type<T>(&self) -> bool {
        self.align_for_type::<T>() == *self
    }
}

impl PtrAlign for usize {
    fn align_forward(&self, align: usize) -> usize {
        (self + (align - 1)) / align * align
    }
}

impl PtrAlign for *const u8 {
    fn align_forward(&self, align: usize) -> *const u8 {
        (*self as usize).align_forward(align) as *const u8
    }
}

impl PtrAlign for *mut u8 {
    fn align_forward(&self, align: usize) -> *mut u8 {
        (*self as usize).align_forward(align) as *mut u8
    }
}

// XXX needs more thought. Should enforce that operations use volatile intrinsics.
pub struct MMIO<T>(*mut T);

impl<T> MMIO<T> {
    pub unsafe fn unwrap(self) -> *mut T {
        self.0
    }

    pub unsafe fn offset(self, count: isize) -> MMIO<T> {
        MMIO(self.0.offset(count))
    }

    pub unsafe fn store(&self, x: T) {
        intrinsics::volatile_store(self.0, x);
    }

    pub unsafe fn load(&self) -> T {
        intrinsics::volatile_load(self.0)
    }
}

pub use rkos_platform::PhysPtr;

/// Physical address.
///
/// Does not implement `Deref` because deref coercions make misuse very easy.
pub struct PhysPtr<T>(*mut T);

impl<T> PhysPtr<T> {
    /// Get a virtual address which refers to this physical address.
    pub fn va(&self) -> *mut T {
        // Nothing should generate physical addresses in the IDMAP range.
        assert!((self.0 as usize) < IDMAP_START);

        (self.0 as usize | IDMAP_START) as *mut T
    }

    pub unsafe fn offset(&self, offset: isize) -> PhysPtr<T> {
        PhysPtr(self.0.offset(offset))
    }

    /// Construct a physical pointer without performing VA translation.
    ///
    /// Unsafe because the PA may be invalid if the provided pointer does not lie within an
    /// identity-mapped region of virtual memory.
    pub unsafe fn id_mapped(p: *mut T) -> PhysPtr<T> {
        PhysPtr(p)
    }

    /// Get the raw physical address referred to.
    ///
    /// Unsafe because there is no guarantee the PA is also a valid VA.
    pub unsafe fn unwrap(self) -> *mut T {
        self.0
    }
}

impl<T> ::core::fmt::Pointer for PhysPtr<T> {
    fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
        write!(f, "{:p}", self.0)
    }
}


pub struct FixedSizeArena<T> {
    start: *mut T,
    len: usize,
    allocated: UnsafeCell<usize>
}

impl<T> FixedSizeArena<T> {
    pub fn new(start: *mut T, len: usize) -> FixedSizeArena<T> {
        // Since we return an Option<&T>, null must not be a valid output.
        assert!(start != ptr::null_mut());

        FixedSizeArena {
            start: start,
            len: len,
            allocated: UnsafeCell::new(0)
        }
    }

    pub fn alloc(&self, value: T) -> Option<&mut T> {
        let allocated = self.allocated.get();
        unsafe {
            if (*allocated + 1) * mem::size_of::<T>() > self.len {
                None
            } else {
                let out = self.start.offset(*allocated as isize);
                *allocated += 1;
                ptr::write(out, value);
                transmute::<*mut T, Option<&mut T>>(out)
            }
        }
    }
}
