//! x86 I/O port access.

#[inline]
pub fn outb(port: u16, value: u8) {
    unsafe { 
        asm!("outb %al, $0"
            : // No outputs
            : "N{dx}"(port), "{al}"(value)
            : // No clobbers
            : "volatile");
    }
}

#[inline]
pub fn inb(port: u16) -> u8 {
    let value: u8;
    unsafe {
        asm!("inb $1, %al"
            : "={al}"(value)
            : "N{dx}"(port)
            : // No clobbers
            : "volatile");
    }
    return value;
}

