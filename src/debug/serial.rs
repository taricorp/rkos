/// Serial debugger features.

// TODO which port, etc needs to be specified in config.
// TODO need a way to explicitly instantiate or something (access to the debugger should be
// serialized, at least for writing strings).
pub struct Debugger;

impl Debugger {
    fn break_into() {
        // Depends on a lot of interrupt/exception stuff.
        unimplemented!();
    }
}

impl core::fmt::Write for Debugger {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        // Needs more thought on the API.
        unimplemented!();
    }
}
