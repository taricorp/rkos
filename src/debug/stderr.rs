use core::fmt;
use ::ctypes::ssize_t;
use ::platform as linux;

pub struct Stderr;

impl fmt::Write for Stderr {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        let s = s.as_bytes();
        let res = linux::syscall::write(linux::STDERR, s) as ssize_t;
        if res as usize == s.len() {
            Ok(())
        } else {
            Err(fmt::Error)
        }
    }
}

impl ::debug::Debugger for Stderr {
    // Probably actually desirable for all x86_64
    #[cfg(all(target_os="linux",target_arch="x86_64"))]
    fn break_into() {
        unsafe {
            asm!("int3" :::: "volatile");
        }
    }
}
