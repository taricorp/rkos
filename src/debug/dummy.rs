//! No-op debugger.

pub struct Dummy;

impl Dummy {
    pub fn break_into() { }
}

impl ::core::fmt::Write for Dummy {
    fn write_str(&mut self, _: &str) -> ::core::fmt::Result {
        Ok(())
    }
}
