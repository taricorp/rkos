//! Interface to a debugger.
//!
//! Set `cfg(debug_backend)` to one of "bochs" or "vga" to select the debugger backend, otherwise
//! the default no-op backend will be used. The Bochs backend works under
//! [Bochs](http://bochs.sourceforge.net/) with the port e9 hack enabled, and the VGA backend
//! doesn't really exist yet.
//!
//! The implementation of `fmt::Write` allows output to be written directly to a debug console:
//!
//! ```
//! write!(&mut Debugger, "Hello from line {}\n", line!()).unwrap();
//! ```
use core::{slice, str};

#[allow(dead_code)]
mod dummy;
#[allow(dead_code)]
mod bochs;
#[allow(dead_code)]
mod stderr;
#[allow(dead_code)]
mod vga;

#[cfg(debug_backend="dummy")]
pub use self::dummy::Dummy as Default;

#[cfg(all(target_os="none",debug_backend="bochs"))]
pub use self::bochs::*;

#[cfg(all(target_os="none",debug_backend="vga"))]
pub use self::vga::*;

#[cfg(debug_backend="serial")]
pub use self::serial::*;

#[cfg(all(debug_backend="stderr"))]
pub use self::stderr::Stderr as Default;

// TODO put somewhere better
pub fn wait_for_attach() {
    unsafe {
        asm!("xor %eax, %eax
              call wait_for_attach_inner"
             ::: "{eax}" : "volatile");
    }
}

use core::fmt::Write;

#[no_mangle]
pub extern "C" fn __rkos_write_debug(p: *const u8, n: u32) -> u32 {
    let mut w = Default;
    let s = unsafe {
        str::from_utf8_unchecked(slice::from_raw_parts(p, n as usize))
    };
    match w.write_str(s) {
        Ok(_) => n,
        Err(_) => 0
    }
}

#[no_mangle]
#[doc(hidden)]
pub unsafe extern fn wait_for_attach_inner() {
    asm!("cmpl $$0, %eax
          je .-3"
         ::: "{eax}" : "volatile");
}

/// Stateless debugger interface.
pub trait Debugger: ::core::fmt::Write {
    /// Halt execution and start the debugger.
    fn break_into();
}
