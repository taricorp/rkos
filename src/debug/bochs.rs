/// Bochs debugger features.
///
/// Both breakpoints and the debug console require that Bochs be configured
/// with the "port E9 hack".
use core;

pub struct Debugger;

impl Debugger {
    fn break_into() {
        // Write 0x8A00, 0x8AE0 to port 0x8A00 to break into the debugger
        unsafe {
            asm!("mov $$0x8A00, %ax
                  mov %ax, %dx
                  outw %ax, %dx
                  mov $$0x8AE0, %ax
                  outw %ax, %dx"
                : /* no outputs */
                : /* no inputs */
                : "{ax}", "{dx}"
                : "volatile");
        }
    }
}

impl core::fmt::Write for Debugger {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        // A byte written to port 0xe9 is printed to the Bochs debug console.
        let addr = s.as_ptr();
        let len = s.len();
        unsafe {
            asm!("rep outsb"
                : /* no outputs */
                : "{dx}"(0xe9), "{rsi}"(addr), "{ecx}"(len)
                : "{dx}", "{rsi}"
                : "volatile")
        }
        Ok(())
    }
}
