
#[cfg(all(target_os="none",target_arch="x86_64"))]
#[path="none-x86_64.rs"]
mod imp;

#[cfg(all(target_os="linux",target_arch="x86_64"))]
#[path="linux-x86_64/mod.rs"]
mod imp;

pub use self::imp::*;
