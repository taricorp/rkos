
pub fn halt() -> ! {
    unsafe {
        asm!("cli
              hlt" :::: "volatile");
        core::intrinsics::unreachable();
    }
}

pub fn bootstrap_vm<A: Allocator>() -> &'static mut [u8] {
    let mut max_pa: usize = 0;
    println!("BIOS memory map:");
    for region in mm::phys::MemoryMap::regions() {
        println!("{}", region);
        if region.is_available() {
            max_pa = cmp::max(max_pa, (region.base + region.length) as usize);
        }
    }

    // Bootstrap memory maps. Memory from 0 through KERNEL_CODE_END is reserved
    // until the heap is bootstrapped completely.
    let mut arena = FixedSizeArena::new(0x100000 as *mut _, 0x100000);
    unsafe {
        let (pml4, _) = mm::virt::get_pt_root();
        let pml4: &mut mm::virt::PML4 = &mut *pml4.unwrap();

        // Unmap everything except bootstrap regions
        mm::virt::unmap_non_reserved(pml4, &arena, mm::meta::KERNEL_CODE_END as usize);
        // Turn on the high-memory identity map so "normal" code can do its stuff
        mm::virt::bootstrap_idmap(pml4, &arena, max_pa);

        // Map stuff into the heap
        mm::virt::bootstrap_heap(&mut arena, mm::phys::MemoryMap::regions().iter());

        let _ = mm::virt::print_vm_map(&mut console::default::Console::open());
    }
    // Return a slice for the heap
    // pmem output would represent at least the low 2MB of physical memory
    unimplemented!();
}

pub fn begin_threading<A: ::mm::alloc::Allocator>(alloc: A, target: extern "C" fn(*mut A) -> !) -> ! {
    // Move stage1 PT arena into the heap (relocating PTs)
    // Allocate interrupt stack
    // Perform stack switch
}
