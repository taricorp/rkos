//! Console displayed on a local VGA adapter.
use core::{intrinsics, self};
use sync::{Mutex, MutexGuard};

static VGA_LOCK: Mutex<()> = Mutex::new(());

pub struct VgaConsole(MutexGuard<'static, ()>);

impl core::fmt::Write for VgaConsole {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        for c in s.chars() {
            put_c(c as u8);
        }
        Ok(())
    }
}

impl super::Console for VgaConsole {
    fn open() -> VgaConsole {
        VgaConsole(VGA_LOCK.lock())
    }

    unsafe fn force_open() -> VgaConsole {
        ::core::mem::uninitialized()
    }

    fn clear(&mut self) {
        unsafe {
            // This is vectorizable but volatile_set_memory only works on bytes
            // and we need to write a 16-bit pattern (and doing a sequence of
            // volatile stores can't be vectorized by the compiler). Thus we do
            // non-volatile stores and force the compiler to commit them.
            // This is not quite ideal since we'd like to permit certain operations
            // to be elided or reordered (cursor location), but no compiler is
            // really capable of doing fences specific to a set of memory locations.
            for i in 0..VGA_SIZE {
                // Nice blue, space character
                *VGA_BASE.offset(i) = 0x1F20;
            }
            intrinsics::atomic_singlethreadfence();
            SCREEN_COL = 0;
            SCREEN_ROW = 0;
        }
    }
}

const VGA_BASE: *mut u16 = 0xB8000 as *mut _;
const VGA_SIZE: isize = SCREEN_WIDTH * SCREEN_HEIGHT;

const SCREEN_WIDTH: isize = 80;
const SCREEN_HEIGHT: isize = 25;

static mut SCREEN_COL: isize = 0;
static mut SCREEN_ROW: isize = 0;


fn put_c(c: u8) {
    unsafe {
        if c != '\n' as u8 {
            // Write character
            *VGA_BASE.offset(SCREEN_WIDTH * SCREEN_ROW + SCREEN_COL) = 0x1F00 | c as u16;
            SCREEN_COL = (SCREEN_COL + 1) % SCREEN_WIDTH;
        } else {
            SCREEN_COL = 0;
        }

        if SCREEN_COL == 0 {
            SCREEN_ROW += 1;
            if SCREEN_ROW >= SCREEN_HEIGHT {
                // Scroll
                intrinsics::copy(VGA_BASE.offset(SCREEN_WIDTH),
                                 VGA_BASE,
                                 (SCREEN_WIDTH * (SCREEN_HEIGHT - 1)) as usize);
                // Clear the last line
                for i in 0..SCREEN_WIDTH {
                    *VGA_BASE.offset(SCREEN_WIDTH * (SCREEN_HEIGHT - 1)).offset(i) = 0x1F20;
                }
                SCREEN_ROW = SCREEN_HEIGHT - 1;
            }
        }
        // See comments in `clear` regarding this fence.
        intrinsics::atomic_singlethreadfence();
    }
}

