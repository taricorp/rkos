//! Local console for messages.
//!
//! The default console is the target of `print!` and `println!`. Access to the console is
//! serialized through a mutex, so messages will never interleave but many threads attempting to
//! write to the console may cause poor performance.
use core::fmt;

pub mod vga;
mod stdio;

/// Write a formatted string to the default console.
#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => ({
        use ::core::fmt::Write;
        use ::console::Console;
        let mut con = ::console::Default::open();
        let _ = write!(&mut con, $($arg)*);
    });
}

/// Write a formatted string to the default console, followed by a newline.
#[macro_export]
macro_rules! println {
    ($fmt:expr) => (print!(concat!($fmt, "\n")));
    ($fmt:expr, $($arg:tt)*) => (print!(concat!($fmt, "\n"), $($arg)*));
}

/// Core functions of a Console.
///
/// This trait is implemented by all concrete consoles (submodules of this one).
pub trait Console: fmt::Write {
    /// Get a handle to the default console.
    ///
    /// May block, since the console may require serialized access.
    fn open() -> Self;
    /// Clear the display.
    fn clear(&mut self);
    /// Get a handle, bypassing any serialization.
    ///
    /// Extremely unsafe. This really only exists so the panic handler can
    /// access a console which might otherwise be locked.
    unsafe fn force_open() -> Self;
}

#[cfg(target_os="none")]
pub use super::vga::VgaConsole as Default;

// New approach
#[cfg(target_os="linux")]
pub use self::stdio::Stdio as Default;

