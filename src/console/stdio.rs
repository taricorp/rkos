use core::fmt;
use ::ctypes::ssize_t;
use ::platform as linux;

pub struct Stdio;

impl fmt::Write for Stdio {
    fn write_str(&mut self, s: &str) -> Result<(), fmt::Error> {
        let s = s.as_bytes();
        let res = linux::syscall::write(linux::STDOUT, s) as ssize_t;
        if res as usize == s.len() {
            Ok(())
        } else {
            Err(fmt::Error)
        }
    }
}

impl ::console::Console for Stdio {
    fn open() -> Stdio {
        Stdio
    }

    fn clear(&mut self) {
        // ANSI escape
        let _ = fmt::Write::write_str(self, "\x1b[2J");
    }

    unsafe fn force_open() -> Stdio {
        // The OS handles concurrency for us, so nothing special to do.
        Stdio
    }
}
