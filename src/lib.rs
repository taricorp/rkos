#![feature(lang_items, asm, const_fn, optin_builtin_traits)]
#![feature(core_intrinsics, collections, alloc)]
// We'd like to use libstd. But we really need a sysroot so we don't need to
// patch it and all of its dependencies. See notes in Cargo.toml.
#![no_std]

extern crate alloc;
extern crate ctypes;
extern crate collections;
extern crate rkos_alloc;
extern crate rkos_alloc_imp;
#[macro_use] extern crate rkos_core_macros;
extern crate rkos_platform as platform;
extern crate rkos_sync as sync;
extern crate rkos_thread as thread;
extern crate rlibc;

use alloc::arc::Arc;
use core::fmt::Write;
use collections::boxed::Box;

pub mod arch;
#[macro_use]
pub mod console;
#[macro_use]
pub mod debug;
//pub mod mm;
pub mod serial;

// This shouldn't be necessary, but ld claims core::Option::<T>::and_then contains
// references to the function. objdump disagrees, so there's something fishy
// going on here.
#[allow(unused_variables)]
#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn _Unwind_Resume() {
    unimplemented!()
}

// Unimplemented for now. Want to use the system stack unwinder,
// which will involve linking against fun things like libgcc
// to get __gcc_personality_v0 or maybe using libunwind.
//
// Refer to the implementation of std::rt::unwind for more hints.
#[lang = "eh_personality"]
extern fn eh_personality() { }

/// Terminate current thread, possibly printing a diagnostic.
#[lang = "panic_fmt"] #[inline(never)] #[cold]
extern fn panic_fmt(args: core::fmt::Arguments,
                    file: &'static str,
                    line: u32) -> ! {
    let mut con = unsafe { ::console::Default::force_open() };
    let _ = write!(con, "PANIC {}:{}: {}\n", file, line, args);
    platform::halt();
}

use console::Console;

#[no_mangle]
pub extern "C" fn kmain() {
    thread::early_init();

    {
        let mut console = console::Default::open();
        console.clear();
    }

    // Set up virtual memory
    let pool = platform::bootstrap_vm_stage1();

    // Initialize system allocator now that we have a memory pool to use.
    ::rkos_alloc_imp::initialize(pool);
    let allocator = ::rkos_alloc::GlobalAllocator;

    // Migrate persistent data into the heap from boot-time memory regions.
    // pmem would be some handle used by the platform for tracking physical memory commit
    // (and claiming more if necessary). I'm not sure about that right now.
    let pmem = Box::new(platform::bootstrap_vm_stage2(&allocator));

    // execute main_on_thread on a managed stack
    thread::begin_threading::<extern "C" fn(*mut platform::PhysMemMeta) -> !>(main_on_thread,
                                                                              Box::into_raw(pmem));
    extern "C" fn main_on_thread(_pmem: *mut platform::PhysMemMeta) -> ! {
        let _pmem = unsafe {
            Box::from_raw(_pmem)
        };
        platform::bootstrap_vm_stage3(rkos_alloc::GlobalAllocator, thread::current_stack_size);

        platform::begin_preempt(thread::preempt_handler);

        const npts: usize = 100000000;
        const nthreads: usize = 4;
        let pi = monte_carlo_pi(npts, nthreads);
        println!("Pi is approximately {}", pi);
        println!("(Computed with {} points over {} threads)", npts * nthreads, nthreads);

        panic!("reached end of kmain");
    }
}

pub mod rand;

fn monte_carlo_pi(npts: usize, nthreads: usize) -> f64 {
    use collections::Vec;
    let mut handles = Vec::with_capacity(nthreads);

    for _ in 0..nthreads {
        handles.push(spawn(move || {
            use rand::distributions::{Range, IndependentSample};
            use rand::{IsaacRng, SeedableRng};

            let tsc = platform::get_timestamp();
            let words: [u32; 2] = [(tsc >> 32) as u32, tsc as u32];
            let mut rng = IsaacRng::from_seed(&words);

            // Test npts points
            let range = Range::new(0f64, 1f64);
            let mut hits: usize = 0;
            for _ in 0..npts {
                let x = range.ind_sample(&mut rng);
                let y = range.ind_sample(&mut rng);

                if x*x + y*y <= 1f64 {
                    hits += 1;
                }
            }

            debug!("Computation thread finished with {}/{} hits", hits, npts);
            hits
        }));
    }

    let nhits: usize = handles.into_iter().map(|h| h.join()).fold(0usize, |a, x| a + x);
    4f64 * (nhits as f64) / (nthreads * npts) as f64
}

struct JoinHandle<T>(Arc<sync::Mutex<Option<T>>>) where T: Send;

impl<T> JoinHandle<T> where T: Send {
    pub fn join(self) -> T {
        loop {
            // Wait until the thread has started, then take its return value
            // once it has finished. We need to manually wait in case the
            // thread has not even started running before we attempt to join.
            {
                let mut g = self.0.lock();
                if g.is_some() {
                    return g.take().unwrap()
                }
            }
            platform::sleep();
        }
    }
}

fn spawn<T: Send + 'static, F: FnOnce() -> T + Send + 'static>(f: F) -> JoinHandle<T> {
    let arc = Arc::new(sync::Mutex::new(None::<T>));
    let st = arc.clone();
    thread::spawn(move || {
        {
            let mut g = st.lock();
            *g = Some(f());
        }
        // Ensure owned state has been dropped before terminating to avoid
        // resource leaks.
        drop(st);
        unsafe {
            thread::terminate();
        }
    }, 5);

    JoinHandle(arc)
}

/*
#[repr(C)]
struct IDT {
    base_lo: u16,
    selector: u16,
    pad0: u8,
    attributes: u8,
    base_mid: u16,
    base_high: u32,
    pad1: u32
}

const TRAP: u8 = 0xF;
const INT: u8 = 0xE;

/*
macro_rules! idt_entry {
    ($func:expr, $ty:expr) => {
        IDT {
            base_lo: ($func as u64) as u16,
            selector: 8,                // Assumed to be in default code segment
            pad0: 0,
            attributes: 0x80 | $ty,     // P-bit
            base_mid: (($func as u64) >> 16) as u16,
            base_high: (($func as u64) >> 32) as u32,
            pad1: 0
        }
    };
}

macro_rules! idt {
    ($name:ident,
        $($func:ident => $ty:expr),*
    ) => (
        static $name: &'static [IDT] = &[ $(
            idt_entry!($func, $ty),
        )* ];
    )
}

idt!{x,
    vector_0 => TRAP
}
*/

#[repr(C)]
struct IDTR {
    limit: u16,
    base: *mut ()
}
*/
