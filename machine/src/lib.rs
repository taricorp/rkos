#![no_std]

/// Maximum per-thread inline stack allocation, in bytes.
///
/// Smaller values will tend to fragment the heap more by creating more
/// aligned allocations, but expanding an inline allocation incurs overhead
/// in reallocation.
// TODO this must be a multiple of VM page size.
pub const THREAD_STACK_INLINE_MAX: usize = 16 << 10;

// TODO want a parameter(s) controlling stack link block coalescing and sizing.
