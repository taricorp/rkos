use core::{cmp, ptr};
use core::marker::PhantomData;
use core::ptr::Unique;
use rkos_alloc::Allocator;
use rkos_sync::Mutex;

// dlmalloc without internal locking.
// We can implement locking just fine ourselves, and ptmalloc wants to refer
// to dlmalloc without internal locks. This does mean the C API here is not
// thread-safe.

#[allow(dead_code, non_camel_case_types)]
mod ffi {
    use ctypes::*;

    #[repr(C)]
    pub struct mallinfo_t {
        arena: size_t,
        ordblks: size_t,
        smblks: size_t,
        hblks: size_t,
        hblkhd: size_t,
        usmblks: size_t,
        fsmblks: size_t,
        uordblks: size_t,
        fordblks: size_t,
        keepcost: size_t
    }

    pub enum mspace {}

    // Parameters to mallopt()
    pub const M_TRIM_THRESHOLD: c_int = -1;
    pub const M_GRANULARITY: c_int = -2;
    pub const M_MMAP_THRESHOLD: c_int = -3;

    extern "C" {
        // None of the following are provided when built with ONLY_MSPACES
        // (as we do).
        /*
        fn dlmalloc(size: size_t) -> *mut u8;
        fn dlfree(p: *mut u8);
        fn dlcalloc(n_elements: size_t, element_size: size_t) -> *mut u8;
        fn dlrealloc(p: *mut u8, n: size_t) -> *mut u8;
        fn dlrealloc_in_place(p: *mut u8, n: size_t) -> *mut u8;
        fn dlmemalign(alignment: size_t, n: size_t) -> *mut u8;
        fn dlposix_memalign(pp: *mut *mut u8, alignment: size_t, n: size_t) -> c_int;
        fn dlvalloc(n: size_t) -> *mut u8;
        fn dlmallopt(parameter_number: c_int, parameter_value: c_int) -> c_int;
        fn dlmalloc_footprint() -> size_t;
        fn dlmalloc_max_footprint() -> size_t;
        fn dlmalloc_footprint_limit() -> size_t;
        fn dlmalloc_set_footprint_limit(bytes: size_t) -> size_t;
        fn dlmalloc_inspect_all(handler: extern "C" fn(*mut u8, *mut u8, size_t, *mut u8),
                                arg: *mut u8);
        fn dlmallinfo() -> mallinfo_t;
        fn dlindependent_calloc(n_elements: size_t, element_size: size_t, chunks: *mut *mut u8) -> *mut *mut u8;
        fn dlindependent_comalloc(n_elements: size_t, sizes: *mut size_t, chunks: *mut *mut u8) -> *mut *mut u8;
        fn dlbulk_free(array: *mut *mut u8, n_elements: size_t) -> size_t;
        fn dlpvalloc(n: size_t) -> *mut u8;
        fn dlmalloc_trim(pad: size_t) -> c_int;
        fn dlmalloc_stats();
        */
        pub fn dlmalloc_usable_size(p: *mut u8) -> size_t;
        // The following are only provided if mspaces are enabled.
        pub fn create_mspace(capacity: size_t, locked: c_int) -> *mut mspace;
        pub fn destroy_mspace(msp: *mut mspace) -> size_t;
        pub fn create_mspace_with_base(base: *mut u8, capacity: size_t, locked: c_int) -> *mut mspace;
        pub fn mspace_track_large_chunks(msp: *mut mspace, enable: c_int) -> c_int;
        pub fn mspace_mallinfo(msp: *mut mspace) -> mallinfo_t;
        pub fn mspace_mallopt(parameter_number: c_int, parameter_value: c_int) -> c_int;

        // Regular functions but for mspaces
        pub fn mspace_malloc(msp: *mut mspace, size: size_t) -> *mut u8;
        pub fn mspace_free(msp: *mut mspace, p: *mut u8);
        pub fn mspace_calloc(msp: *mut mspace, n_elements: size_t, element_size: size_t) -> *mut u8;
        pub fn mspace_realloc(msp: *mut mspace, p: *mut u8, n: size_t) -> *mut u8;
        pub fn mspace_realloc_in_place(msp: *mut mspace, p: *mut u8, n: size_t) -> *mut u8;
        pub fn mspace_memalign(msp: *mut mspace, alignment: size_t, n: size_t) -> *mut u8;
        pub fn mspace_independent_calloc(msp: *mut mspace, n_elements: size_t,
                                     element_size: size_t, chunks: *mut *mut u8) -> *mut *mut u8;
        pub fn mspace_independent_comalloc(msp: *mut mspace, n_elements: size_t,
                                           sizes: *mut size_t, chunks: *mut *mut u8) -> *mut *mut u8;
        pub fn mspace_bulk_free(msp: *mut mspace, array: *mut *mut u8, n_elements: size_t) -> size_t;
        pub fn mspace_footprint(msp: *mut mspace, ) -> size_t;
        pub fn mspace_max_footprint(msp: *mut mspace, ) -> size_t;
        pub fn mspace_footprint_limit(msp: *mut mspace, ) -> size_t;
        pub fn mspace_set_footprint_limit(msp: *mut mspace, bytes: size_t) -> size_t;
        pub fn mspace_inspect_all(msp: *mut mspace,
                                  handler: extern "C" fn(*mut u8, *mut u8, size_t, *mut u8),
                                  arg: *mut u8);
    }
}

pub struct DlMalloc<'a>(Mutex<Unique<ffi::mspace>>, PhantomData<&'a mut [u8]>);

const MIN_ALIGN: usize = 8;

impl<'a> DlMalloc<'a> {
    pub fn new(pool: &'a mut [u8]) -> DlMalloc<'a> {
        DlMalloc(Mutex::new(Self::create_with_pool(pool)), PhantomData)
    }

    pub const unsafe fn uninitialized() -> DlMalloc<'static> {
        DlMalloc(Mutex::new(Unique::new(ptr::null_mut())), PhantomData)
    }

    pub fn is_initialized(&self) -> bool {
        (**self.0.lock()) != ptr::null_mut()
    }

    pub fn initialize_with_pool(&self, pool: &'a mut [u8]) {
        let mut g = self.0.lock();
        *g = Self::create_with_pool(pool);
    }

    fn create_with_pool(pool: &'a mut [u8]) -> Unique<ffi::mspace> {
        unsafe {
            let ms = ffi::create_mspace_with_base(pool.as_mut_ptr(), pool.len(), 0);
            if ms == ptr::null_mut() {
                panic!("{}-byte pool is too small for DlMalloc", pool.len());
            }
            Unique::new(ms)
        }
    }
}

impl<'a> Allocator for DlMalloc<'a> {
    fn allocate(&self, size: usize, align: usize) -> *mut u8 {
        debug_assert!(self.is_initialized());
        let g = self.0.lock();
        unsafe {
            if align <= MIN_ALIGN {
                ffi::mspace_malloc(**g, size)
            } else {
                ffi::mspace_memalign(**g, align, size)
            }
        }
    }

    fn free(&self, ptr: *mut u8, _old_size: usize, _align: usize) {
        debug_assert!(self.is_initialized());
        let g = self.0.lock();
        unsafe {
            ffi::mspace_free(**g, ptr);
        }
    }

    fn reallocate(&self, ptr: *mut u8, old_size: usize, size: usize, align: usize) -> *mut u8 {
        debug_assert!(self.is_initialized());
        let g = self.0.lock();
        unsafe {
            if align <= MIN_ALIGN {
                // Can use native realloc function
                ffi::mspace_realloc(**g, ptr, size)
            } else {
                // Must emulate to ensure alignment. Try to do it in-place first, which
                // will implicitly preserve alignment.
                if ffi::mspace_realloc_in_place(**g, ptr, size) == ptr {
                    ptr
                } else {
                    // Couldn't do it in-place. Create a new block, return null on failure
                    // but don't touch the input block.
                    let outp = ffi::mspace_memalign(**g, align, size);
                    if outp != ptr::null_mut() {
                        ptr::copy_nonoverlapping(ptr, outp, cmp::min(old_size, size));
                        ffi::mspace_free(**g, ptr);
                    }
                    outp
                }
            }
        }
    }

    fn reallocate_inplace(&self, ptr: *mut u8, old_size: usize, size: usize, _align: usize) -> usize {
        debug_assert!(self.is_initialized());
        let g = self.0.lock();
        unsafe {
            let newp = ffi::mspace_realloc_in_place(**g, ptr, size);
            // Returns the input pointer on success
            if newp != ptr {
                old_size
            } else {
                size
            }
        }
    }

    fn usable_size(&self, size: usize, _align: usize) -> usize {
        // mspace_usable_size takes a pointer. We can't implement this interface.
        size
    }
}

#[cfg(test)]
mod test {
    use std::prelude::v1::*;
    use std::{slice, ptr};
    use super::*;
    use rkos_alloc::Allocator;

    #[test]
    fn basics() {
        let increments = (0..256usize).map(|x| x as u8).collect::<Vec<_>>();
        let mut pool = vec![0u8; 16384];
        let m = DlMalloc::new(&mut *pool);

        // Small enough to fulfill
        let d = m.allocate(256, 0);
        assert!(d != ptr::null_mut());
        // We can write to the allocation
        unsafe {
            ptr::copy_nonoverlapping(increments.as_ptr(), d, 256);
            assert_eq!(slice::from_raw_parts(d, 256), &*increments);
        }
        // Reallocating preserves data
        let d = m.reallocate(d, 256, 128, 0);
        assert!(d != ptr::null_mut());
        assert_eq!(unsafe { slice::from_raw_parts(d, 128) }, &increments[..128]);
    }
}
