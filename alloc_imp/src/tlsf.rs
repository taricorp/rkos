use core::cmp;
use core::marker::PhantomData;
use core::ptr::{self, Unique};
use rkos_sync::Mutex;
use rkos_alloc::Allocator;

#[allow(non_camel_case_types, dead_code)]
mod ffi {
    use ctypes::*;

    pub enum _tlsf_t {}
    pub enum _pool_t {}
    pub type tlsf_t = *mut _tlsf_t;
    pub type pool_t = *mut _pool_t;
    pub type tlsf_walker = extern "C" fn(*mut u8, size_t, c_int, *mut u8);

    extern "C" {
        pub fn tlsf_create_with_pool(mem: *mut u8, bytes: size_t) -> tlsf_t;
        pub fn tlsf_destroy(tlsf: tlsf_t);
        pub fn tlsf_get_pool(tlsf: tlsf_t) -> pool_t;

        pub fn tlsf_add_pool(tlsf: tlsf_t, mem: *mut u8, bytes: size_t) -> pool_t;
        pub fn tlsf_remove_pool(tlsf: tlsf_t, pool: pool_t);

        pub fn tlsf_malloc(tlsf: tlsf_t, bytes: size_t) -> *mut u8;
        pub fn tlsf_memalign(tlsf: tlsf_t, align: size_t, bytes: size_t) -> *mut u8;
        pub fn tlsf_realloc(tlsf: tlsf_t, ptr: *mut u8, size: size_t) -> *mut u8;
        pub fn tlsf_free(tlsf: tlsf_t, ptr: *mut u8);

        pub fn tlsf_block_size(ptr: *mut u8) -> size_t;

        pub fn tlsf_size() -> size_t;
        pub fn tlsf_align_size() -> size_t;
        pub fn tlsf_block_size_min() -> size_t;
        pub fn tlsf_block_size_max() -> size_t;
        pub fn tlsf_pool_overhead() -> size_t;
        pub fn tlsf_alloc_overhead() -> size_t;

        pub fn tlsf_walk_pool(pool: pool_t, walker: tlsf_walker, user: *mut u8);
        pub fn tlsf_check(tlsf: tlsf_t) -> c_int;
        pub fn tlsf_check_pool(pool: pool_t) -> c_int;
    }
}

#[no_mangle]
pub extern "C" fn __tlsf_assert_fail(message: *const u8) {
    let message = unsafe {
        let mut strlen = 0usize;
        let mut t = message;
        while *t != 0 {
            strlen += 1;
            t = t.offset(1);
        }
        ::core::slice::from_raw_parts(message, strlen)
    };
    let message = ::core::str::from_utf8(message).expect("invalid utf8 in tlsf_assert");

    panic!("TLSF assertion failed: {}", message);
}

// TLSF is not threadsafe, so we need to synchronize access to this allocator.
pub struct Tlsf<'a>(Mutex<Unique<ffi::_tlsf_t>>, PhantomData<&'a mut [u8]>);

#[cfg(target_pointer_width="32")]
const MIN_ALIGN: usize = 4;
#[cfg(target_pointer_width="64")]
const MIN_ALIGN: usize = 8;

impl<'a> Tlsf<'a> {
    pub fn new(pool: &'a mut [u8]) -> Tlsf<'a> {
        Tlsf(Mutex::new(Self::create_with_pool(pool)),
             PhantomData)
    }

    pub const unsafe fn uninitialized() -> Tlsf<'a> {
        Tlsf(Mutex::new(Unique::new(ptr::null_mut())), PhantomData)
    }

    pub fn is_initialized(&self) -> bool {
        *(*self.0.lock()) != ptr::null_mut()
    }

    pub fn initialize_with_pool(&self, pool: &'a mut [u8]) {
        let mut g = self.0.lock();
        *g = Self::create_with_pool(pool);
    }

    fn create_with_pool(pool: &'a mut [u8]) -> Unique<ffi::_tlsf_t> {
        unsafe {
            Unique::new(
                ffi::tlsf_create_with_pool(pool.as_mut_ptr(), pool.len())
            )
        }
    }
}

impl<'a> Allocator for Tlsf<'a> {
    fn allocate(&self, size: usize, align: usize) -> *mut u8 {
        debug_assert!(self.is_initialized());
        let g = self.0.lock();
        unsafe {
            if align <= MIN_ALIGN {
                ffi::tlsf_malloc(**g, size)
            } else {
                ffi::tlsf_memalign(**g, align, size)
            }
        }
    }

    fn free(&self, ptr: *mut u8, _old_size: usize, _align: usize) {
        debug_assert!(self.is_initialized());
        let g = self.0.lock();
        unsafe {
            ffi::tlsf_free(**g, ptr);
        }
    }

    fn reallocate(&self, ptr: *mut u8, old_size: usize, size: usize, align: usize) -> *mut u8 {
        debug_assert!(self.is_initialized());
        let g = self.0.lock();
        unsafe {
            if align <= MIN_ALIGN {
                // Can use native realloc function
                ffi::tlsf_realloc(**g, ptr, size)
            } else {
                // Must emulate to ensure alignment
                let outp = ffi::tlsf_memalign(**g, align, size);
                if outp != ptr::null_mut() {
                    ptr::copy_nonoverlapping(ptr, outp, cmp::min(old_size, size));
                    ffi::tlsf_free(**g, ptr);
                }
                outp
            }
        }
    }

    fn reallocate_inplace(&self, _ptr: *mut u8, old_size: usize, _size: usize, _align: usize) -> usize {
        // Not supported; always fails.
        old_size
    }

    fn usable_size(&self, size: usize, _align: usize) -> usize {
        // Don't have enough information to say
        size
    }
}

