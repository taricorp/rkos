//! Memory allocator implementations.
//!
//! All allocators are threadsafe, so they can be shared between threads
//! and consequently (provided threads use the same allocator) objects
//! may be shared across threads.
#![feature(allocator, const_fn, unique)]
#![allocator]
#![no_std]

#[cfg(test)]
#[macro_use]
extern crate std;

extern crate ctypes;
extern crate rkos_alloc;
extern crate rkos_sync;
extern crate rkos_thread as thread;

pub mod dlmalloc;
pub mod tlsf;

use rkos_alloc::Allocator;

static SYSTEM_ALLOCATOR: dlmalloc::DlMalloc<'static> = unsafe {
    dlmalloc::DlMalloc::uninitialized()
};

/// Initialize the global allocator with the given memory pool.
pub fn initialize(pool: &'static mut [u8]) {
    assert!(!SYSTEM_ALLOCATOR.is_initialized());
    SYSTEM_ALLOCATOR.initialize_with_pool(pool);
}

// Implicit runtime functions.
// These must not be provided when testing in a hosted environment, otherwise the thing
// we're testing will be used by the runtime.

/// Allocate `size` bytes with minimum alignment `align` in bytes.
#[cfg(not(test))]
#[no_mangle]
pub extern fn __rust_allocate(size: usize, align: usize) -> *mut u8 {
    assert!(!thread::is_in_interrupt());
    SYSTEM_ALLOCATOR.allocate(size, align)
}

/// Free memory at `ptr` with allocation size and alignment.
#[no_mangle]
#[cfg(not(test))]
pub extern fn __rust_deallocate(ptr: *mut u8, old_size: usize, align: usize) {
    assert!(!thread::is_in_interrupt());
    SYSTEM_ALLOCATOR.free(ptr, old_size, align)
}

/// Reallocate memory at `ptr` with current size `_old_size` to become
/// `size` bytes large with alignment `align`.
#[no_mangle]
#[cfg(not(test))]
pub extern fn __rust_reallocate(ptr: *mut u8, old_size: usize, size: usize,
                                align: usize) -> *mut u8 {
    assert!(!thread::is_in_interrupt());
    SYSTEM_ALLOCATOR.reallocate(ptr, old_size, size, align)
}

/// Reallocate in-place, returning the new allocation size.
#[no_mangle]
#[cfg(not(test))]
pub extern fn __rust_reallocate_inplace(ptr: *mut u8, old_size: usize,
                                        size: usize, align: usize) -> usize {
    assert!(!thread::is_in_interrupt());
    SYSTEM_ALLOCATOR.reallocate_inplace(ptr, old_size, size, align)
}

#[no_mangle]
#[cfg(not(test))]
pub extern fn __rust_usable_size(size: usize, align: usize) -> usize {
    assert!(!thread::is_in_interrupt());
    SYSTEM_ALLOCATOR.usable_size(size, align)
}
