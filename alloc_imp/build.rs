extern crate gcc;
extern crate rkos_platform;

use std::env;

fn main() {
    let out_dir = env::var("OUT_DIR").expect("Cargo OUT_DIR not set?!");

    gcc::compile_library("libtlsf.a", &["tlsf-3.0/tlsf.c"]);

    let mut dl = ::gcc::Config::new();
    dl.define("USE_DL_PREFIX", None)
      .define("ONLY_MSPACES", Some("1"))
      .define("HAVE_MMAP", Some("0"))
      .define("HAVE_MREMAP", Some("0"))
      .define("LACKS_TIME_H", None)         // Don't use time(3)
      .define("NO_MALLOC_STATS", None)      // `printf`s stats
      .define("LACKS_UNISTD_H", None)       // sysconf for pagesize, define PAGESIZE instead
      .define("PAGESIZE", Some(&format!("{}", rkos_platform::VM_PAGE_SIZE)))
      .define("MALLOC_FAILURE_ACTION", None)    // Default sets errno; don't
      .define("abort", Some("__rkos_platform_abort"))
      .file("dlmalloc-2.8.6/malloc.c")
      .compile("libdlmalloc.a");

    // For ptmalloc:
    // build ptmalloc3.c, link against dlmalloc. It uses mspaces only internally.
    // We will need to adapt the sysdeps things to provide hooks into Rust.
    // Need to define things; look into these:
    //  * USE_TSD_DATA_HACK
    //  * _REENTRANT
    //  * USE_LOCKS (in dlmalloc)
    //  * USE_STARTER
    //  * FOOTERS (in dlmalloc)
    //
    // Other things:
    //  * atfork
    //  * mmap
    //  * supporting MORECORE would be nice; providing a page allocator ends up
    //    being like the interface jemalloc wants (a "chunk allocator" to feed
    //    it).
    //
    // For jemalloc:
    // This doesn't have an mspace-like API, instead using a "chunk provider"
    // to get hunks of address space, such as with the arena.<i>.chunk_hooks
    // mallctl.
    //
    // An example is in libhpx, via the jemalloc mailing list:
    // http://comments.gmane.org/gmane.comp.lib.jemalloc/1178
    // Here it uses the mallocx API to specify an arena which has been
    // pre-created with the correct chunk hooks.
    //
    // This requires additional complexity in that we need some kind of bitmap
    // for tracking chunk allocations, but also permits us to do things like
    // perform memory ballooning under hypervisors.

    // Other allocators to look at supporting:
    //  * ptmalloc (largely a wrapper over dlmalloc for thread safety!)
    //  * nedmalloc
    //  * tcmalloc
    //  * jemalloc

    println!("cargo:rustc-link-search=native={}", out_dir);
    // Apparently gcc::Config::compile provides these for us. Don't cause symbol
    // duplication by linking the libraries twice.
    //println!("cargo:rustc-link-lib=static=tlsf");
    //println!("cargo:rustc-link-lib=static=dlmalloc");
}

