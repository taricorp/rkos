//! x86_64 freestanding thread state
//!
//! This is the "real" implementation, used in non-test configurations.

use core::{mem, ptr};
use core::ptr::Unique;

use super::{TcbHandle, platform};

fn allocate_stack(bytes: usize) -> *mut u8 {
    use ::rkos_alloc::{Allocator, GlobalAllocator};
    assert!(bytes % platform::VM_PAGE_SIZE == 0,
            "Stack allocation must be a multiple of VM page size");
    let out = GlobalAllocator.allocate(bytes, platform::VM_PAGE_SIZE);
    if out == ptr::null_mut() {
        panic!("Failed to allocate stack of {} bytes", bytes);
    }
    out
}

#[repr(simd)]
struct __Align16(u64, u64);

/// Thread control block.
///
/// Includes information about memory allocated to the thread (stacks and
/// thread locals), its machine state (if the thread is not currently running),
/// and various metadata.
///
/// TCBs are allocated within the stack of a thread, so a thread requires only
/// one allocation in most cases and if stack memory needs are small this
/// reduces the thread's overall memory needs.
///
/// It would be nice to support arbitrary allocators here, but there's no good
/// (safe) way to support runtime polymorphism (vtables) while ensuring threads
/// don't outlive their allocators here.
#[repr(C)]
pub struct Tcb {
    /// Pointer to this TCB.
    ///
    /// This is a shortcut over getting the actual value of GSBASE, which
    /// requires either an MSR read or RDGSBASE, which require kernel mode
    /// (needs a syscall on hosted configurations) or the appropriate extended
    /// CPU feauture flag (with a CPUID check).
    pub selfp: *mut Tcb,
    /// Total size of the stack, in bytes.
    ///
    /// Equal to the sum of `stack_internal.size` and
    /// `sum(iter(stack_link.block.size))`.
    pub stack_size: usize,
    /// Address and size of the initial allocation for the stack. The TCB is embedded
    /// in this allocation.
    pub stack_internal: SBlock,
    /// Head of the linked list of additional stack allocations.
    pub stack_link: Unique<SLink>,
    /// Next entry in wait queue, or null if thread is executing.
    ///
    /// If the ready queue becomes lockless, this needs to become an
    /// atomic pointer.
    pub queue_next: TcbHandle,
    /// Unique static ID for this thread.
    pub id: super::Id,
    /// `true` if the thread is currently panicking.
    pub panicking: bool,
    /// `true` if the thread is currently being interrupted.
    ///
    /// This is mostly used for sanity-checking so functions that are not
    /// interrupt-safe can verify they are not being called from interrupts.
    pub in_interrupt: bool,
    /// `true` if the thread should be terminated when preempted.
    ///
    /// This field is checked when a thread gets descheduled.
    pub terminating: bool,
    pub gprs: Gprs,
    // fxsave requires 16-byte alignment on fpstate, but we pack
    // the TCB into the top of the thread's stack which is by definition
    // page-aligned.
    __align: [__Align16; 0],
    pub fpstate: [u8; 512],
}

pub struct SBlock {
    pub size: usize,
    pub ptr: Unique<u8>
}

impl SBlock {
    fn new(ptr: *mut u8, size: usize) -> SBlock {
        SBlock {
            size: size,
            ptr: unsafe { Unique::new(ptr) }
        }
    }

    const fn null() -> SBlock {
        SBlock {
            size: 0,
            ptr: unsafe { Unique::new(ptr::null_mut()) }
        }
    }

    fn destroy(self) {
        use ::rkos_alloc::{Allocator, GlobalAllocator};
        GlobalAllocator.free(*self.ptr, self.size, platform::VM_PAGE_SIZE);
    }
}

impl ::core::fmt::Debug for SBlock {
    fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
        write!(f, "Slink {{ {:#x} bytes @ {:p} }}", self.size, self.ptr)
    }
}

pub struct SLink {
    pub next: Unique<SLink>,
    pub block: SBlock
}

pub const DUMMY_TCB: Tcb = Tcb {
    selfp: ptr::null_mut(),
    stack_size: 0,
    stack_internal: SBlock::null(),
    stack_link: unsafe { Unique::new(ptr::null_mut()) },
    queue_next: unsafe { TcbHandle::new(ptr::null_mut()) },
    id: super::Id::no_thread(),
    panicking: false,
    in_interrupt: false,
    terminating: false,
    gprs: Gprs {
        rax: 0, rbx: 0, rcx: 0, rdx: 0,
        rsi: 0, rdi: 0, rbp: 0, rsp: 0,
        r8:  0, r9:  0, r10: 0, r11: 0,
        r12: 0, r13: 0, r14: 0, r15: 0,
        rip: 0, rflags: 0
    },
    __align: [],
    fpstate: [0u8; 512]
};


macro_rules! field_offset(
    ($field:ident in $t:ty) => ({
        // Use a function so we can silence unused_unsafe when this macro is
        // expanded inside an unsafe context.
        #[allow(unused_unsafe)]
        #[inline(always)]
        fn unsafe_offset(p: *const $t) -> usize {
            // Safe because we never actually deref p
            (&((unsafe { &*p }).$field) as *const _ as usize) - (p as usize)
        }
        let bogus = ptr::null::<$t>();
        unsafe_offset(bogus)
    })
);

/// x86_64 general purpose registers.
///
/// These are the registers (excluding floating-point state) preserved
/// on thread switch.
#[repr(C)]
#[derive(Debug, Default)]
pub struct Gprs {
    pub rax: u64, pub rbx: u64, pub rcx: u64, pub rdx: u64,
    pub rsi: u64, pub rdi: u64, pub rbp: u64, pub rsp: u64,
    pub r8:  u64, pub r9:  u64, pub r10: u64, pub r11: u64,
    pub r12: u64, pub r13: u64, pub r14: u64, pub r15: u64,
    pub rip: u64,
    pub rflags: u64,
}


impl ::core::fmt::Debug for Tcb {
    fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
        write!(f, "Tcb {{ gprs: {:?}, panicking: {:?}, stack_internal: {:?}, \
                   stack_link: {:p}, fpstate: <elided> }}",
               self.gprs, self.panicking, self.stack_internal, self.stack_link)
    }
}


impl Tcb {
    /// Create a thread with stack size `size` in pages which when switched to will execute `f`.
    ///
    /// Stack size must be nonzero.
    pub fn create<F>(size: usize, f: F, values: F::Values) -> TcbHandle 
            where F: super::ConstructibleAbi {
        // Stack must be large enough for a TCB and be nonzero-sized.
        // TODO must also be large enough for all parameters on the stack
        assert!(mem::size_of::<Tcb>() <= platform::VM_PAGE_SIZE);
        assert!(size > 0);
        let size = size * platform::VM_PAGE_SIZE;

        // Allocate a buffer for the stack
        let sbuf = allocate_stack(size) as *mut u8;
        // Write the TCB into the stack head region.
        unsafe {
            let tcbp = sbuf.offset(size as isize).offset(-(mem::size_of::<Tcb>() as isize)) as *mut Tcb;
            ptr::write(tcbp, Tcb {
                selfp: tcbp,
                stack_size: size,
                stack_internal: SBlock::new(sbuf, size),
                stack_link: Unique::new(ptr::null_mut()),
                id: super::Id::new(),
                panicking: false,
                in_interrupt: false,
                terminating: false,
                queue_next: TcbHandle::new(ptr::null_mut()),
                gprs: Default::default(),
                __align: [],
                fpstate: mem::uninitialized()
            });

            f.build_machine_state(&mut *tcbp, values);

            // FP state: just inherit from this context which is assumed to be correct
            // (x87 state and MXCSR) rather than build a fresh one.
            debug_assert!(&mut (*tcbp).fpstate as *mut _ as usize & 0xF == 0,
                          "fpstate must be 16-byte aligned for fxsave");
            asm!("fxsave64 ($0)"
                 : /* No outputs */
                 : "r"(&mut (*tcbp).fpstate)
                 : "memory"
                 : "volatile");

            TcbHandle::new(tcbp)
        }
    }

    pub fn grow_stack(_tcb: TcbHandle) -> TcbHandle {
        use ::rkos_alloc::{Allocator, GlobalAllocator};
        let tcb = unsafe { _tcb.get() };

        debug!("grow_stack: internal {:?}, total size = {:#x}", tcb.stack_internal, tcb.stack_size);
        debug!("(inline max = {:#x} bytes)", ::machine::THREAD_STACK_INLINE_MAX);

        if tcb.stack_internal.size < ::machine::THREAD_STACK_INLINE_MAX {
            // Grow the inline allocation by a page.
            unsafe {
                let orig_alloc = *tcb.stack_internal.ptr;
                let orig_size = tcb.stack_internal.size;
                let new_size = orig_size + platform::VM_PAGE_SIZE;
                let newp = GlobalAllocator.reallocate(orig_alloc, orig_size, new_size, platform::VM_PAGE_SIZE);
                if newp.is_null() {
                    // return _tcb
                    panic!("OOM attempting to grow stack");
                }

                // Move the existing data to the back of the new allocation, since it
                // was placed at the front.
                ptr::copy(newp, newp.offset(platform::VM_PAGE_SIZE as isize), orig_size);

                // And we must update internal pointers
                let tcbp = newp.offset(new_size as isize).offset(-(mem::size_of::<Tcb>() as isize)) as *mut Tcb;
                let tcb = &mut *tcbp;
                tcb.selfp = tcb;
                tcb.stack_size += platform::VM_PAGE_SIZE;
                tcb.stack_internal.size += platform::VM_PAGE_SIZE;
                tcb.stack_internal.ptr = Unique::new(newp);
                assert!(tcb.queue_next.is_null(), "Resizing stack of queued thread is super-dangerous");
                Unique::new(tcbp)
            }
        } else {
            // Add another block to the linked allocations.
            unimplemented!()
        }
    }

    /// Get the offset in bytes from top of stack to the non-TCB area.
    fn top_offset() -> isize {
        -(mem::size_of::<Tcb>() as isize)
    }

    /// Get a pointer to the top of the user stack region (below the TCB).
    pub fn get_stack_top(&self) -> *mut u8 {
        unsafe {
            // The internal allocation is always top of stack so we don't
            // need to follow any chain of additional allocations.
            self.stack_internal.ptr.offset(self.stack_internal.size as isize)
                                   .offset(Self::top_offset())
        }
    }

    pub unsafe fn destroy(_tcb: TcbHandle) {
        let tcb: *mut Tcb = *_tcb;
        // Free the internal stack allocation
        ptr::read(tcb).stack_internal.destroy();

        // Walk linked allocations and free all of those.
        let mut link = ptr::read(tcb).stack_link;
        while !link.is_null() {
            unimplemented!();
        }
    }
}

/// Return `true` if the current thread is panicking.
pub fn is_panicking() -> bool {
    const FIELD_OFS: usize = 52;
    assert_eq!(field_offset!(panicking in Tcb), FIELD_OFS);

    let out: bool;
    unsafe {
        // Safe because deref through GS cannot be interrupted
        asm!("movb %gs:52, $0"
             : "=r"(out));
    }
    out
}

/// Return `true` if the current thread is in an interrupt context.
pub fn is_in_interrupt() -> bool {
    const FIELD_OFS: usize = 53;
    assert_eq!(field_offset!(in_interrupt in Tcb), FIELD_OFS);

    let out: bool;
    unsafe {
        // Safe because deref through GS cannot be interrupted.
        asm!("movb %gs:53, $0"
             : "=r"(out));
    }
    out
}

pub fn set_in_interrupt(x: bool) {
    const FIELD_OFS: usize = 53;
    assert_eq!(field_offset!(in_interrupt in Tcb), FIELD_OFS);

    unsafe {
        // Safe because deref through GS cannot be interrupted.
        asm!("movb $0, %gs:53"
             : /* No outputs */
             : "r"(x)
             : "memory");
    }
}

pub unsafe fn set_terminating(x: bool) {
    assert_eq!(field_offset!(terminating in Tcb), 54);
    asm!("movb $0, %gs:54" : /* No outputs */ : "r"(x) : "memory");
}

/// Get a pointer to the TCB, not dependent on having a stack mapping.
///
/// The pointer should not be dereferenced unless you're sure it won't
/// move. A preempted thread's TCB may be moved, so ensure you won't
/// be preempted while holding this pointer.
unsafe fn get_tcb_pointer() -> *mut Tcb {
    assert!(field_offset!(selfp in Tcb) == 0, "TCB layout mismatch");
    let p: *mut Tcb;
    // Deref through GS cannot be interrupted but the TCB may move,
    // invalidating the output pointer.
    asm!("mov %gs:0, $0"
         : "=r"(p));

    p
}

/// Return true if the system is running on a thread.
///
/// This indicates whether there is a valid stack mapping, used to
/// to TCB accesses through the top of stack rather than via its
/// heap address.
pub fn have_thread_context() -> bool {
    unsafe {
        // Safe because we don't deref this pointer.
        !get_tcb_pointer().is_null()
    }
}

/// Get a pointer to the TCB for the running thread.
///
/// This pointer is into the heap and may move when the thread is preempted.
/// You should probably only use this function when in an interrupt context.
pub fn get_active_tcb() -> TcbHandle {
    assert!(is_in_interrupt(), "TCB handle is only safe to take while thread is preempted");
    assert!(have_thread_context());
    let p = if have_thread_context() {
        // Safe because we ensured we're in an interrupt (we won't be preempted)
        unsafe { get_tcb_pointer() }
    } else {
        platform::read_gs_base() as *mut Tcb
    };
    assert!(!p.is_null(), "TCB pointer not initialized");
    unsafe {
        TcbHandle::new(p)
    }
}

/// Get the ID of the current thread.
pub fn current_id() -> super::Id {
    assert_eq!(field_offset!(id in Tcb), 48);
    let out: u32;
    unsafe {
        // Safe because deref through GS cannot be interrupted.
        asm!("movl %gs:48, $0"
             : "=r"(out));
    }
    super::Id(out)
}

/// Get the size of the current thread's stack, in bytes.
pub fn current_stack_size() -> usize {
    assert_eq!(field_offset!(stack_size in Tcb), 8);
    let out: usize;
    unsafe {
        // Safe because deref through GS cannot be interrupted.
        asm!("movq %gs:16, $0"
             : "=r"(out));
    }
    out
}

// === SCHEDULING ===
//
// Scheduling must not allocate memory (because it happens in interrupt contexts).
// 
// Wait queues are the exclusive owners of TCBs when queued. A thread will never
// be executing at the same time as it is present in a wait queue.
//
// This implementation uses locking because it's easier. Would prefer a lockless queue.
// We can't use a rkos_sync::Mutex because that would introduce a circular dependency.
struct ReadyQueue {
    head: *mut Tcb,
    tail: *mut Tcb
}
// The pointers are enforced as unique internally and access always goes
// through a CoreMutex (UnsafeCell).
unsafe impl Send for ReadyQueue {}

static READY_QUEUE: super::CoreMutex<ReadyQueue> = super::CoreMutex::new(ReadyQueue {
    head: ptr::null_mut(),
    tail: ptr::null_mut()
});

/// Yield the provided thread and return a new thread to begin executing.
pub fn select_new_to_run(tcb: Option<TcbHandle>) -> TcbHandle {
    debug_assert!(have_thread_context());
    if let Some(tcb) = tcb {
        ready_queue_push(tcb);
    }   
    get_runnable()
}

pub fn get_runnable() -> TcbHandle {
    loop {
        match ready_queue_pop() {
            None => ::platform::sleep(),
            Some(tcb) => return tcb
        }
    }
}

fn ready_queue_pop() -> Option<TcbHandle> {
    let mut q = READY_QUEUE.lock();

    // If the queue is empty give up.
    if q.head == ptr::null_mut() {
        None
    } else {
        // Grab the old head and swap in its next link as the new head.
        let head = unsafe { &mut *q.head };
        let null_handle = unsafe {
            TcbHandle::new(ptr::null_mut())
        };
        q.head = *mem::replace(&mut head.queue_next, null_handle);

        // If we took the last entry, clear tail.
        if q.head == ptr::null_mut() {
            q.tail = ptr::null_mut();
        }
        Some(unsafe {
            TcbHandle::new(head)
        })
    }
}

/// Add the given thread to the ready queue, to be executed when a CPU is free.
pub fn ready_queue_push(tcb: TcbHandle) {
    let mut q = READY_QUEUE.lock();

    let tail = q.tail;
    q.tail = *tcb;

    if tail == ptr::null_mut() {
        // Queue was empty. New entry is both head and tail.
        q.head = q.tail;
    } else {
        // Queue was not empty. Link old tail to new tail.
        let tail = unsafe { &mut *tail };
        tail.queue_next = tcb;
    }
}
