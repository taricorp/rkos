#![no_std]
#![feature(asm, repr_simd, const_fn, unique)]

extern crate rkos_alloc;
#[macro_use] extern crate rkos_core_macros;
extern crate rkos_machine as machine;
extern crate rkos_platform as platform;

use core::{mem, ptr, slice};
use core::cell::UnsafeCell;
use core::cmp::min;
use core::ops::{Deref, DerefMut};
use core::sync::atomic::{AtomicBool, AtomicUsize, Ordering};

use platform::PreemptState;
use rkos_alloc::GlobalAllocator;

#[cfg(not(target_env=""))]
#[path="hosted.rs"]
mod imp;

#[cfg(all(target_env="", target_arch="x86_64"))]
#[path="x86_64.rs"]
mod imp;

pub use imp::*;

/// Default size of a thread's stack on creation, in pages.
pub const INITIAL_THREAD_STACK: usize = 4;

/// Thread ID
///
/// There are some reserved values, used by mutexes for indicating current state.
#[derive(PartialEq, Eq, Debug)]
pub struct Id(pub u32);

// Initial value is one more than max reserved value.
static TID_MAX: AtomicUsize = AtomicUsize::new(2);

impl Id {
    fn new() -> Id {
        let v = Id(TID_MAX.fetch_add(1, Ordering::Relaxed) as u32);
        if v == Self::poison() {
            // We're overflowing TIDs. The value we just got is poison and
            // any subsequent ones may collide with existing threads.
            // TODO maintain a bitmap for claimed TIDs with configurable
            // maximum count so we can wrap and avoid collision.
            panic!("Unable to assign new TID: pool exhausted");
        }
        v
    }

    pub const fn unlocked() -> Id {
        Id(0)
    }
    pub const fn no_thread() -> Id {
        Id(1)
    }
    pub const fn poison() -> Id {
        Id(!0)
    }
}

/// Moving wrapper for TCBs.
///
/// Use of a TcbHandle helps ensure that consumers of TCBs cannot
/// accidentally copy pointers.
pub type TcbHandle = core::ptr::Unique<Tcb>;

struct CoreMutex<T>(AtomicBool, UnsafeCell<T>);
struct CoreMutexGuard<'a, T>(&'a CoreMutex<T>) where T: 'a;

impl<T> CoreMutex<T> {
    pub const fn new(x: T) -> CoreMutex<T> {
        CoreMutex(AtomicBool::new(false), UnsafeCell::new(x))
    }

    pub fn lock<'a>(&'a self) -> CoreMutexGuard<'a, T> {
        loop {
            let prev = self.0.compare_and_swap(false, true, Ordering::AcqRel);
            if prev == false {
                break;
            }
        }
        CoreMutexGuard(self)
    }
}

unsafe impl<T: Send> Sync for CoreMutex<T> { }

impl<'a, T> Drop for CoreMutexGuard<'a, T> {
    fn drop(&mut self) {
        (self.0).0.store(false, Ordering::Release);
    }
}

impl<'a, T> Deref for CoreMutexGuard<'a, T> {
    type Target = T;
    fn deref(&self) -> &T {
        unsafe {
            &*(self.0).1.get()
        }
    }
}

impl<'a, T> DerefMut for CoreMutexGuard<'a, T> {
    fn deref_mut(&mut self) -> &mut T {
        unsafe {
            &mut *(self.0).1.get()
        }
    }
}

/// Map in the stack for a thread.
///
/// Unsafe because if there is a stack currently mapped it may be partially or completely
/// unmapped.
unsafe fn map_thread_stack(tcb: &Tcb) {
    let mut block_start: *mut u8 = *tcb.stack_internal.ptr;
    let mut block_size: usize = tcb.stack_internal.size;
    let mut block_va = platform::THREAD_STACK_TOP as *mut u8;
    let mut link_p = *tcb.stack_link;

    loop {
        block_va = block_va.offset(-(block_size as isize));
        let block_src = slice::from_raw_parts_mut(block_start, block_size);
        platform::map_va_range(block_src, block_va as *mut u8);

        if link_p.is_null() {
            break;
        }

        let link: &imp::SLink = &*link_p;
        block_start = *link.block.ptr;
        block_size = link.block.size;
        link_p = *link.next;
    }

    debug!("Mapped thread stack of {} bytes for thread {:?}", tcb.stack_size, tcb.id);
}

/// Unmap the stack for a thread identified by its TCB.
///
/// Unsafe because this does not guarantee the TCB corresponds to the mapped stack.
unsafe fn unmap_thread_stack(tcb: &Tcb) {
    let sm_start = platform::THREAD_STACK_TOP - tcb.stack_size;
    let stack = slice::from_raw_parts(sm_start as *const u8, tcb.stack_size);

    platform::unmap_va_range(stack);
}

/// Trait for functions which we can construct a stack frame for.
pub trait ConstructibleAbi {
    type Values;
    /// Arranges to call `self` with `Values` as a parameter within the given TCB's context.
    ///
    /// That is, when the thread is executed it will call `self(Values)`.
    fn build_machine_state(self, &mut Tcb, Self::Values);
}

// C ABI requires we pass only raw pointers, but this function really takes
// ownership over the pointed value, since it ends up on the stack of the
// new thread.
// TODO we should heap-allocate the parameter
// TODO can that raw pointer param be a reference or Unique<T> instead?
impl<T> ConstructibleAbi for extern "C" fn(*mut T) -> ! {
    type Values = *mut T;

    fn build_machine_state(self, tcb: &mut Tcb, value: *mut T) {
        // Pointers fit in registers, so we don't need to pass anything as
        // a memory reference. Nothing crazy necessary for the stack, beyond
        // ensuring correct alignment.

        // Get the VA of stack top in an active thread. This is the top of stack
        // minus TCB size, since the TCB itself is placed at the top of the stack.
        let stack_top = platform::THREAD_STACK_TOP - mem::size_of::<Tcb>();
        assert!(mem::align_of::<Tcb>() & (mem::align_of::<Tcb>() - 1) == 0,
                "TCB alignment is assumed to be a power of two");
        assert!(mem::size_of::<Tcb>() & (mem::align_of::<Tcb>() - 1) == 0,
                "TCB is assumed to have alignment compatible with size");

        // rsp at entry: per the C ABI rsp+8 on function entry is 16-byte aligned.
        // It does not need to be writable or readable though.
        // Shift down from p_storage only if not aligned.
        let rsp_offset = if (stack_top + 8) & 15 != 0 {
            // Must align downward
            8
        } else {
            // Already aligned satisfactorily
            0
        };
        tcb.gprs.rsp = stack_top as u64 - rsp_offset;
        debug!("RSP at {:#x}, offset {} from ToS", tcb.gprs.rsp, rsp_offset);

        // rip at entry: the target function
        tcb.gprs.rip = unsafe {
            mem::transmute::<_, u64>(self)
        };

        // Parameter 1: pointer value
        tcb.gprs.rdi = value as u64;
    }
}

impl<T> ConstructibleAbi for T where T: FnOnce() -> () + Send + 'static {
    type Values = ();

    fn build_machine_state(self, tcb: &mut Tcb, _: ()) {
        // Target is Rust ABI, so we need to dispatch through C ABI.
        // We'll put self on the heap (to avoid wasting stack space for
        // passing by-value which would involve two copies, one in the C
        // parameter area and one in the Rust function's stack frame), then
        // hop to a C ABI translator (since the Rust ABI isn't defined).
        use rkos_alloc::{GlobalAllocator, Allocator};
        
        // Box self
        let b = GlobalAllocator.allocate(mem::size_of::<Self>(), mem::align_of::<Self>()) as *mut Self;
        debug!("Boxed closure for spawn at {:p}", b);
        unsafe {
            ptr::write(b, self);
        }
        // Arrange to call the ABI translator
        //abi_bridge::<T>.build_machine_state(tcb, b);
        <extern "C" fn(*mut T) -> ! as ConstructibleAbi>::build_machine_state(abi_bridge::<T>, tcb, b);

        extern "C" fn abi_bridge<T: FnOnce()>(p: *mut T) -> ! {
            debug!("In ABI bridge, closure reading from {:p}", p);
            let f: T = unsafe {
                let f = ptr::read(p);
                GlobalAllocator.free(p as *mut u8, mem::size_of::<T>(), mem::align_of::<T>());
                f
            };
            f();
            unreachable!();
        }
    }
}


/*
// need an allocator
static ALL_THREADS: AtomicQueue<*mut Tcb> = ::sync::AtomicQueue::new();
*/

/*
pub fn select_new_to_run(tcb: *mut Tcb) -> *mut Tcb {
    unimplemented!();
    /*
    ALL_THREADS.push(tcb);
    ALL_THREADS.pop()
    */
}
*/

/// Copy state of preempted thread to TCB.
fn save_thread_context(ps: &PreemptState, tcb: &mut Tcb) {
    let tc = &mut tcb.gprs;

    tc.rax = ps.get_rax();
    tc.rbx = ps.get_rbx();
    tc.rcx = ps.get_rcx();
    tc.rdx = ps.get_rdx();
    tc.rdi = ps.get_rdi();
    tc.rsi = ps.get_rsi();
    tc.rsp = ps.get_rsp();
    tc.rbp = ps.get_rbp();
    tc.r8 = ps.get_r8();
    tc.r9 = ps.get_r9();
    tc.r10 = ps.get_r10();
    tc.r11 = ps.get_r11();
    tc.r12 = ps.get_r12();
    tc.r13 = ps.get_r13();
    tc.r14 = ps.get_r14();
    tc.r15 = ps.get_r15();
    // Not-GPRs
    tc.rip = ps.get_rip();
    tc.rflags = ps.get_eflags();

    tcb.fpstate = match ps.get_fpstate() {
        None => [0; 512],
        Some(s) => *s
    }
}

/// Copy TCB saved context to interrupt restore context (ucontext).
fn restore_thread_context(tcb: &Tcb, ps: &mut PreemptState) {
    let tc = &tcb.gprs;

    // GPRs (and stack pointer)
    ps.set_rax(tc.rax);
    ps.set_rbx(tc.rbx);
    ps.set_rcx(tc.rcx);
    ps.set_rdx(tc.rdx);
    ps.set_rdi(tc.rdi);
    ps.set_rsi(tc.rsi);
    ps.set_rsp(tc.rsp);
    ps.set_rbp(tc.rbp);
    ps.set_r8(tc.r8);
    ps.set_r9(tc.r9);
    ps.set_r10(tc.r10);
    ps.set_r11(tc.r11);
    ps.set_r12(tc.r12);
    ps.set_r13(tc.r13);
    ps.set_r14(tc.r14);
    ps.set_r15(tc.r15);

    // Not-GPRs
    ps.set_rip(tc.rip);
    ps.set_eflags(tc.rflags);
    // Segment registers are application-managed.
    platform::write_gs_base(tcb as *const _ as u64);

    match ps.get_fpstate_mut() {
        None => { /* Odd, but okay. No state to deal with. */ }
        Some(p) => { *p = tcb.fpstate; }
    }
}

/// Perform early initialization so items attempting to use a thread context
/// have something to use.
pub fn early_init() {
    platform::write_gs_base(&DUMMY_TCB as *const _ as u64);
}

/// Don't use this.
///
/// This sets up the threading subsystem and arranges to call `f(params)` on
/// a thread. The global allocator must be initialized. Anything stored on
/// the current stack at invocation will be deleted without dropping.
pub fn begin_threading<F>(f: F, params: F::Values) -> !
        where F: ConstructibleAbi {
    assert!(!have_thread_context(), "begin_threading must be called only once");
    // Create a TCB, invoke the platform function to switch stacks
    // then map in the new thread's stack and switch back to it.
    
    // Set up thread to be executing f(*params)
    let tcb = Tcb::create(INITIAL_THREAD_STACK, f, params);
    // Invoke map_new_stack on alternate stack with page fault handler registered
    unsafe {
        platform::begin_threading(map_new_stack, tcb, pagefault_handler, GlobalAllocator);
    }
    unreachable!();

    fn map_new_stack(tcb: TcbHandle, state: &mut PreemptState) {
        unsafe {
            map_thread_stack(tcb.get());
            restore_thread_context(tcb.get(), state);
        }
        // Will return into the modified PreemptState on the freshly-mapped stack
    }
}

/// Handles preemption.
pub fn preempt_handler(ps: &mut PreemptState) {
    set_in_interrupt(true);

    // Stash thread state in the TCB
    let mut tcb = get_active_tcb();
    let orig_tid = unsafe { tcb.get().id.0 };
    let should_terminate = unsafe { tcb.get().terminating };
    debug!("preempted thread should terminate? {:?}", should_terminate);

    unsafe {
        save_thread_context(ps, tcb.get_mut());
        unmap_thread_stack(tcb.get());

        let tcb = if should_terminate {
            Tcb::destroy(tcb);
            select_new_to_run(None)
        } else {
            select_new_to_run(Some(tcb))
        };

        map_thread_stack(tcb.get());
        restore_thread_context(tcb.get(), ps);
        debug!("PREEMPT schedule({}) -> {}", orig_tid, tcb.get().id.0);
    }

    set_in_interrupt(false);
}

/// Handles page faults for stack adjustment.
///
/// Return `true` if the fault has been handled, otherwise `false` in which case
/// the platform is free to crash in the meaningful fashion of its choice.
fn pagefault_handler(fault_addr: *mut u8, _ps: &mut PreemptState) -> bool {
    set_in_interrupt(true);

    let fault_addrs = fault_addr as usize;
    let out = if fault_addrs < platform::THREAD_STACK_TOP &&
       fault_addrs >= (platform::THREAD_STACK_TOP - min(current_stack_size() + 4096,
                                                        platform::THREAD_STACK_MAX_SIZE)) {
        // TODO terminate that thread
        panic!("Stack overflow in {:?}: stack size {:#x} bytes", current_id(), current_stack_size());
        /*
        // Fault is inside the guard page and stack is able to grow. Do it.
        debug!("Page fault inside guard page at {:p}; growing stack", fault_addr);
        // Grow the stack. This may move the allocation so we need to remap
        // the stack and update GSBASE.
        let mut tcb = get_active_tcb();
        unsafe {
            // Safe to work with becase we were allowed to get an owning handle for
            // the thread's TCB.
            tcb = Tcb::grow_stack(tcb);
            platform::write_gs_base(*tcb as u64);
            map_thread_stack(tcb.get());
        }

        true
        */
    } else {
        // Fault is either:
        //  * Thread stack overflow and cannot grow
        //  * Interrupt stack overflow
        //  * A scary bug
        // TODO we'd like stack probes so we can check if it's actually a stack overflow.
        false
    };

    set_in_interrupt(false);
    out
}


/// Spawn a new thread executing `f`, with `stack_size` pages allocated for
/// its stack (minimum 1).
///
/// If the stack overflows, the thread will be terminated.
pub fn spawn<F>(f: F, stack_size: usize)
        where F: FnOnce() -> () + Send + 'static {
    ready_queue_push(Tcb::create(stack_size, f, ()));
}

/// Yield the CPU so another thread may execute immediately.
pub fn schedule() {
    // Just fire a preemption interrupt manually
    platform::raise_preempt_interrupt();
}

/// Terminates the current thread.
///
/// Unsafe because this does not unwind the stack and thus any live bindings
/// will not be dropped.
pub unsafe fn terminate() -> ! {
    // Set the termination flag then force scheduling.
    // TODO If we can't allocate in interrupt context (the status quo),
    // there actually needs to be a reaper thread that does the freeing.
    // We won't actually kill the thread for now.
    //set_terminating(true);
    schedule();

    // Something's wrong if we get resumed
    panic!("Thread resumed after termination");
}
