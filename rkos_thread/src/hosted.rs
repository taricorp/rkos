//! Hosted thread state.
//!
//! Used largely for testing, where we *want* to use libstd for a change.
extern crate std;

/// Get the ID of the current thread.
pub fn current_id() -> super::Id {
    // libc doesn't have bindings to this syscall, so we need to do it outselves.
    let tid: u64;
    const _NR_GETTID: u64 = 186;
    unsafe {
        asm!("syscall"
             : "={rax}"(tid)
             : "{rax}"(_NR_GETTID) 
             : "rcx", "r11", "memory");
    }
    tid as super::Id
}

/// Returns `true` if the current thread is panicking.
pub fn is_panicking() -> bool {
    self::std::thread::panicking()
}

pub fn current_stack_size() -> usize {
    unimplemented!();
}
