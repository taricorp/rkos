
struct PhysPtr<T>(*mut T);

impl<T> From<*mut T> for PhysPtr<T> {
    fn from(x: *mut T) -> PhysPtr {
        PhysPtr(x)
    }
}
