use core::marker::PhantomData;
use alloc::Allocator;

pub const THREAD_STACK_TOP: usize = 0;

/// Pause the machine in a low-power state.
///
/// The amount of time this will wait for is implementation-defined, but usually
/// until an interrupt fires.
pub fn sleep() {
    unimplemented!()
}

/// Halt the current CPU without performing any cleanup.
pub fn halt() -> ! {
    unimplemented!()
}

/// A handle for accessing physical memory.
///
/// It must be possible to convert a virtual address to a physical address for
/// mapping memory, but it need not be possible to go the other direction.
/// Some platforms may use such a feature for their own purposes, but it is
/// not part of the required API.
pub struct PhysPtr<T>(PhantomData<*mut T>);

impl<T> PhysPtr<T> {
    /// Get a pointer to a location in physical memory given its virtual address.
    ///
    /// Return `None` if there is no physical memory backing the given pointer.
    pub fn from_ptr(_p: *mut T) -> Option<PhysPtr<T>> {
        unimplemented!()
    }

    /// Return an iterator over contiguous regions of physical memory backing
    /// the given slice, ignoring unmapped regions.
    pub fn backing_regions(_region: &mut [T]) -> BackingPARegions<T> {
        unimplemented!()
    }

    /// Return `true` iff the entire contents of the given slice are backed by
    /// mapped virtual memory.
    pub fn va_range_is_mapped(_range: &[T]) -> bool {
        unimplemented!()
    }
}

impl PhysPtr<u8> {
    /// Create a VA mapping of `len` bytes at the given virtual address `p`
    /// backed by the physical memory at `self`.
    ///
    /// Replaces any existing mappings at the given VA, and the given length
    /// must be a multiple of the physical page size.
    ///
    /// The region of physical memory is assumed to be contiguous. Recommend
    /// getting those pointers via `PhysPtr::backing_regions`.
    pub unsafe fn map_at_va(&self, _p: *mut u8, _len: usize) {
        unimplemented!()
    }
}

/// An iterator over regions of contiguous physical memory.
///
/// See `PhysPtr::backing_regions`.
///
/// Returns pairs of a physical address and number of properly-aligned items
/// that will fit in the region based at that address. For virtual addresses
/// within the requested range that are not backed by physical memory (that is,
/// the virtual addresses are not mapped), no result is yielded.
pub struct BackingPARegions<T>(PhantomData<T>);

impl<T> Iterator for BackingPARegions<T> {
    type Item = (PhysPtr<T>, usize);

    fn next(&mut self) -> Option<Self::Item> {
        unimplemented!()
    }
}

/// Initialize the virtual memory system, returning a slice representing the
/// global heap's virtual memory mapping.
///
/// This makes no guarantees about the address space beyond having a usable
/// heap. In particular, there may be information stored in boot-time memory
/// regions which overlap with address ranges that are assumed to be available
/// during normal operation.
pub fn bootstrap_vm_stage1() -> &'static mut [u8] {
    unimplemented!()
}

pub struct PhysMemMeta;

/// Clean up boot-time memory regions, moving any data necessary into the heap
/// provided by the passed allocator.
///
/// This does not necessarily unmap those regions, but the system is guaranteed
/// not to hold any references into those areas at this point so they can be
/// replaced or removed at a later stage. The exception here is the boot
/// stack, which we may still be executing on.
pub fn bootstrap_vm_stage2<A: Allocator>(_alloc: &A) -> PhysMemMeta {
    unimplemented!()
}

/// Discard boot-time memory regions, unmapping those areas which are still
/// mapped.
///
/// This is called after a threading context has been set up, so the system
/// stack (the only thing that might be in use after stage2) is guaranteed to
/// no longer be in use.
///
/// An implementation may choose to augment the provided heap with the memory
/// which was formerly mapped into these regions, but is not required to.
pub fn bootstrap_vm_stage3<A: Allocator, F: FnOnce() -> usize>(_alloc: &A, _get_stack_size: F) {
    unimplemented!()
}

/// Set the value of the GS register, used as a pointer to the active thread's
/// TCB.
pub fn write_gs_base(_x: *mut u8) {
    unimplemented!()
}
