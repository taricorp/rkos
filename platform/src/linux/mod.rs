//! Linux implementation of platform abstractions.

pub mod syscall;
pub mod threading;

use alloc::Allocator;
use ctypes::{c_int, off_t};
use core::marker::PhantomData;
use core::{intrinsics, slice};
use core::sync::atomic::{AtomicIsize, AtomicUsize, Ordering};
use super::HEAP_BASE;
use syscall::FileHandle;

// Top of userspace, where the active thread's stack beinggs.
//
// Per TASK_SIZE_MASK defined in the kernel, this is "47 bits minus one guard page".
pub const THREAD_STACK_TOP: usize = (1 << 47) - 4096;

pub const STDIN: FileHandle = unsafe { FileHandle::from_fd(0) };
pub const STDOUT: FileHandle = unsafe { FileHandle::from_fd(1) };
pub const STDERR: FileHandle = unsafe { FileHandle::from_fd(2) };

/// Pause the machine in a low-power state.
pub fn sleep() {
    // nanosleep suspends the thread either until the time is elapsed or a
    // signal is delivered. Under normal operation we use signals like hardware
    // interrupts, so we can use an arbitrarily long sleep time since we're
    // really emulating hardware interrupts to wake up.
    syscall::nanosleep(1, 0);
}

#[no_mangle]
pub extern "C" fn __rkos_platform_abort() -> ! {
    halt()
}

pub fn halt() -> ! {
    syscall::kill(syscall::getpid(), syscall::Signal::SIGABRT);
    unsafe {
        intrinsics::unreachable();
    }
}

/// fd for the file backing the heap.
static HEAP_FD: AtomicIsize = AtomicIsize::new(-1);
/// Size in bytes of the file backing the heap.
///
/// The heap memory region is thus from HEAP_BASE through
/// HEAP_BASE + HEAP_SIZE.
static HEAP_SIZE: AtomicUsize = AtomicUsize::new(0);

#[derive(Debug)]
pub struct PhysPtr<T> {
    fileno: FileHandle,
    offset: off_t,
    _mark: PhantomData<*mut T>
}

impl<T> PhysPtr<T> {
    pub fn from_ptr(p: *mut T) -> Option<PhysPtr<T>> {
        let p = p as usize;
        let heap_size = HEAP_SIZE.load(Ordering::Acquire);
        if p < HEAP_BASE || p > (super::HEAP_BASE + heap_size) {
            // Only the heap is backed by a mappable file (disregarding /proc/self/exe)
            // because it shouldn't be necessary to map bits of the binary to arbitrary
            // VAs.
            None
        } else {
            Some(PhysPtr {
                fileno: unsafe { FileHandle::from_fd(HEAP_FD.load(Ordering::Relaxed) as c_int) },
                offset: (p - HEAP_BASE) as off_t,
                _mark: PhantomData
            })
        }
    }

    pub fn backing_regions(region: &mut [T]) -> BackingPARegions<T> {
        BackingPARegions {
            done: false,
            base: region.as_mut_ptr(),
            len: region.len()
        }
    }

    pub fn va_range_is_mapped(region: &[T]) -> bool {
        let p = region.as_ptr();
        let endp = unsafe {
            p.offset(region.len() as isize)
        };

        let heap_size = HEAP_SIZE.load(Ordering::Relaxed);

        (p as usize) >= HEAP_BASE &&
            endp as usize <= (HEAP_BASE + heap_size)
    }
}

impl PhysPtr<u8> {
    pub unsafe fn map_at_va(&self, p: *mut u8, len: usize) {
        use syscall::{mmap, PROT_READ, PROT_WRITE, MAP_SHARED, MAP_FIXED};
        debug!("map_at_va({:?}) to {:p} len {:#x}", self, p, len);
        let sl = slice::from_raw_parts(p, len);
        mmap(sl, self.fileno, self.offset, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_FIXED);
    }
}

pub struct BackingPARegions<T> {
    base: *mut T,
    len: usize,
    done: bool
}

impl<T> Iterator for BackingPARegions<T> {
    type Item = (PhysPtr<T>, usize);

    fn next(&mut self) -> Option<Self::Item> {
        if self.done {
            return None;
        }

        let mut start = self.base;
        unsafe {
            // Get base in range of heap mappings.
            // Looping for ranges is easy but inefficient here.
            while (start as usize) < HEAP_BASE {
                start = start.offset(1);
                self.len -= 1;
            }

            // Get end in range of heap mappings.
            while (start.offset(self.len as isize) as usize) > (HEAP_BASE + HEAP_SIZE.load(Ordering::Relaxed)) {
                self.len -= 1;
            }
        }

        // There is only one contiguous mapping, so we'll only ever return one PA range.
        // After returning it, we're done.
        self.done = true;

        Some((PhysPtr::from_ptr(start).unwrap(), self.len))
    }
}

pub unsafe fn unmap_va_range(range: &[u8]) {
    syscall::munmap(range);
}

pub fn bootstrap_vm_stage1() -> &'static mut [u8] {
    use self::syscall::{
        memfd_create, ftruncate, mmap,
        MemfdFlags,
        PROT_READ, PROT_WRITE, MAP_SHARED, MAP_FIXED
    };
    const HEAP_TARGET_SIZE: usize = 64 << 20;
    HEAP_SIZE.store(HEAP_TARGET_SIZE, Ordering::Release);

    let fd = memfd_create(b"heap\0", MemfdFlags::empty());
    ftruncate(fd, HEAP_SIZE.load(Ordering::Relaxed));
    HEAP_FD.store(fd.into(), Ordering::Release);

    let heap_slice = unsafe {
        slice::from_raw_parts_mut(HEAP_BASE as *mut u8, HEAP_SIZE.load(Ordering::Relaxed))
    };
    let heap_ptr = unsafe {
        mmap(heap_slice, fd, 0, PROT_READ | PROT_WRITE,
             MAP_SHARED | MAP_FIXED)
    };
    if heap_ptr as usize != HEAP_BASE {
        panic!("heap mmap failed ({:p})", heap_ptr);
    }

    debug!("Mapped {:#x} bytes from memfd {:?} for heap at {:p}",
           HEAP_SIZE.load(Ordering::Relaxed), fd, heap_ptr);
    heap_slice
}

pub struct PhysMemMeta;

pub fn bootstrap_vm_stage2<A: Allocator>(_alloc: &A) -> PhysMemMeta {
    // Nothing to do! We don't store any state in boot memory that will
    // later get unmapped.
    PhysMemMeta
}

pub fn bootstrap_vm_stage3<A, F>(_alloc: A, get_stack_size: F)
        where A: Allocator, F: FnOnce() -> usize {
    // Since we're now executing on a managed stack, we can unmap the stack the kernel gave us as
    // well as anything else we had at boot-time.
    // Unmap the following ranges:
    //  * 0 -> interrupt stack bottom
    //  * signal stack top -> kimage base
    //  * kimage top -> heap base
    //  * heap top -> thread stack bottom
    //
    // In practice we'll make some assumptions that the first range is already unmapped
    // and the second has zero size. Also assume that there is nothing mapped in the
    // third region, so we only need to unmap between the end of the heap and
    // bottom of executing stack.
    
    let unmap_start = HEAP_BASE + HEAP_SIZE.load(Ordering::Relaxed);
    let unmap_end = THREAD_STACK_TOP - get_stack_size();
    unsafe {
        let region = slice::from_raw_parts(unmap_start as *const u8, unmap_end - unmap_start);
        syscall::munmap(region);
    }
}

pub fn write_gs_base(x: u64) {
    use syscall::{arch_prctl, ArchPrctlOperation};
    arch_prctl(ArchPrctlOperation::SetGS(x as *mut u8));
}

pub fn read_gs_base() -> u64 {
    use syscall::{arch_prctl, ArchPrctlOperation};
    arch_prctl(ArchPrctlOperation::GetGS).unwrap() as u64
}

pub use threading::{begin_threading, PreemptState, begin_preempt, raise_preempt_interrupt};
