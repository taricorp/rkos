//! Linux syscall interfaces.
#![allow(dead_code, non_camel_case_types)]

use core::{mem, ptr};
use ctypes::*;

// === syscall ABI ===

#[inline]
unsafe fn syscall0(nr: u64) -> u64 {
    let ret: u64;
    asm!("syscall"
         : "={rax}"(ret)
         : "{rax}"(nr)
         : "rcx", "r11", "memory");
    ret
}

#[inline]
unsafe fn syscall1(nr: u64, a1: u64) -> u64 {
    let ret: u64;
    asm!("syscall"
         : "={rax}"(ret)        // Output
         : "{rax}"(nr),
           "{rdi}"(a1)          // Inputs
         : "rcx", "r11", "memory"   // Clobbers
         );
    ret
}

#[inline]
unsafe fn syscall2(nr: u64, a1: u64, a2: u64) -> u64 {
    let ret: u64;
    asm!("syscall"
         : "={rax}"(ret)        // Output
         : "{rax}"(nr),
           "{rdi}"(a1),
           "{rsi}"(a2)          // Inputs
         : "rcx", "r11", "memory"   // Clobbers
         );
    ret
}

#[inline]
unsafe fn syscall3(nr: u64, a1: u64, a2: u64, a3: u64) -> u64 {
    let ret: u64;
    asm!("syscall"
         : "={rax}"(ret)        // Output
         : "{rax}"(nr),
           "{rdi}"(a1),
           "{rsi}"(a2),
           "{rdx}"(a3)          // Inputs
         : "rcx", "r11", "memory"   // Clobbers
         );
    ret
}

#[inline]
unsafe fn syscall4(nr: u64, a1: u64, a2: u64, a3: u64, a4: u64) -> u64 {
    let ret: u64;
    asm!("syscall"
         : "={rax}"(ret)        // Output
         : "{rax}"(nr),
           "{rdi}"(a1),
           "{rsi}"(a2),
           "{rdx}"(a3),
           "{r10}"(a4)          // Inputs
         : "rcx", "r11", "memory"   // Clobbers
         );
    ret
}
    
#[inline]
unsafe fn syscall6(nr: u64, a1: u64, a2: u64, a3: u64, a4: u64, a5: u64, a6: u64) -> u64 {
    let ret: u64;
    asm!("syscall"
         : "={rax}"(ret)        // Output
         : "{rax}"(nr),
           "{rdi}"(a1),
           "{rsi}"(a2),
           "{rdx}"(a3),
           "{r10}"(a4),
           "{r8}"(a5),
           "{r9}"(a6)          // Inputs
         : "rcx", "r11", "memory"   // Clobbers
         );
    ret
}

// Syscall numbers (in rax at invocation of `syscall` instruction)
pub const NR_WRITE: u64 = 1;
pub const NR_MMAP: u64 = 9;
pub const NR_MUNMAP: u64 = 11;
pub const NR_SIGACTION: u64 = 13;
pub const NR_SIGPROCMASK: u64 = 14;
pub const NR_SIGRETURN: u64 = 15;
pub const NR_MADVISE: u64 = 28;
pub const NR_NANOSLEEP: u64 = 35;
pub const NR_GETITIMER: u64 = 36;
pub const NR_SETITIMER: u64 = 38;
pub const NR_GETPID: u64 = 39;
pub const NR_EXIT: u64 = 60;
pub const NR_KILL: u64 = 62;
pub const NR_FTRUNCATE: u64 = 77;
pub const NR_SIGALTSTACK: u64 = 131;
pub const NR_ARCH_PRCTL: u64 = 158;
pub const NR_MEMFD_CREATE: u64 = 319;

// Error codes. Most syscalls return a negative value on error where the error
// code is the opposite of one of these.
#[repr(i8)]
enum Errno {
    EPERM = 1,
    ENOENT = 2,
    ESRCH = 3,
    EINTR = 4,
    EIO = 5,
    ENXIO = 6,
    E2BIG = 7,
    ENOEXEC = 8,
    EBADF = 9,
    ECHILD = 10,
    EAGAIN = 11,
    ENOMEM = 12,
    EACCESS = 13,
    EFAULT = 14,
    ENOTBLK = 15,
    EBUSY = 16,
    EEXIST = 17,
    EXDEV = 18,
    ENODEV = 19,
    ENOTDIF = 20,
    EISDIR = 21,
    EINVAL = 22,
    ENFILE = 23,
    EMFILE = 24,
    ENOTTY = 25,
    ETXTBSY = 26,
    EFBIG = 27,
    ENOSPC = 28,
    ESPIPE = 29,
    EROFS = 30,
    EMLINK = 31,
    EPIPE = 32,
    EDOM = 33,
    ERANGE = 34
}

// === Time functions ===

#[repr(C)]
#[derive(Debug)]
pub struct timeval {
    /// seconds
    pub tv_sec: c_long,
    /// microseconds
    pub tv_usec: c_long
}

#[repr(C)]
#[derive(Debug)]
pub struct itimerval {
    pub it_interval: timeval,
    pub it_value: timeval
}

#[repr(C)]
#[derive(Debug)]
pub struct timespec {
    pub tv_sec: c_long,     // time_t
    pub tv_nsec: c_long
}

/// Suspend the current thread for at least `seconds + nanoseconds * 10^-9` seconds,
/// or until interrupted.
///
/// Return `true` if the sleep terminated early because it was interrupted, otherwise
/// `false`.
///
/// This does not expose the output parameter to see how much time is left in the sleep
/// if interrupted which is provided by the system.
pub fn nanosleep(seconds: u64, nanoseconds: u64) -> bool {
    let ts = timespec {
        tv_sec: seconds as c_long,
        tv_nsec: nanoseconds as c_long
    };
    let res = unsafe {
        syscall2(NR_NANOSLEEP, &ts as *const _ as u64, ptr::null_mut::<timespec>() as u64) as c_int
    };
    // Returns EINTR if interrupted, 0 otherwise unless EFAULT or EINVAL.
    if res != 0 && res != -(Errno::EINTR as c_int) {
        panic!("nanosleep failed with status {}", res);
    }
    res != 0
}

#[repr(u8)]
pub enum ITimerType {
    /// Runs in real time.
    Real = 0,
    /// Runs only when the process is executing.
    Virtual = 1,
    /// Runs when the process is executing or the system is executing on its
    /// behalf.
    Prof = 2
}

pub fn setitimer(which: ITimerType, value: &itimerval) {
    let res = unsafe {
        syscall3(NR_SETITIMER, which as u64,
                 value as *const _ as u64,
                 ptr::null::<itimerval>() as u64) as c_int
    };
    if res != 0 {
        panic!("setitimer failed with status {}", res);
    }
}

pub fn getitimer(which: ITimerType) -> itimerval {
    let mut v: itimerval;
    let res = unsafe {
        v = mem::uninitialized();
        syscall2(NR_GETITIMER, which as u64, &mut v as *mut _ as u64)
    };
    if res != 0 {
        panic!("setitimer failed with status {}", res);
    }
    v
}

// === Memory mappings manipulation ===

bitflags! {
    pub flags MmapProtection: ::ctypes::c_int {
        const PROT_NONE = 0,
        const PROT_READ = 1,
        const PROT_WRITE = 2,
        const PROT_EXEC = 4,
    }
}
bitflags! {
    pub flags MmapFlags: ::ctypes::c_int {
        const MAP_SHARED = 1,
        const MAP_PRIVATE = 2,
        const MAP_FIXED = 0x10,
        const MAP_ANONYMOUS = 0x20,
        const MAP_NORESERVE = 0x4000,
    }
}

/// Map some part of a file into virtual memory.
///
/// Unsafe because this replaces any existing mappings for the specified range.
pub unsafe fn mmap(range: &[u8],
                   fd: FileHandle, offset: off_t,
                   prot: MmapProtection, flags: MmapFlags) -> *mut u8 {
    let ret = syscall6(NR_MMAP, range.as_ptr() as u64, range.len() as u64,
                       prot.bits() as u64, flags.bits() as u64, fd.0 as u64, offset as u64) as i64;
    if ret < 0 {
        panic!("mmap failed with status {}", ret);
    }
    ret as *mut u8
}

/// Unmap virtual memory.
///
/// There need not be anything mapped into the given region for this to
/// succeed, and it may unmap portions of an existing mapping (that is,
/// mapped regions are not indivisible).
///
/// Unsafe because it makes addresses invalid.
pub unsafe fn munmap(range: &[u8]) {
    let res = syscall2(NR_MUNMAP, range.as_ptr() as u64, range.len() as u64);
    if res != 0 {
        panic!("munmap failed with status {}", res);
    }
}

#[repr(i8)]
pub enum MadviseAdvice {
    MADV_NORMAL = 0,
    MADV_RANDOM = 1,
    MADV_SEQUENTIAL = 2,
    MADV_WILLNEED = 3,
    MADV_DONTNEED = 4,
    MADV_REMOVE = 9,
    MADV_DONTFORK = 10,
    MADV_DOFORK = 11,
    MADV_HWPOISON = 100,
    MADV_SOFT_OFFLINE = 101,
    MADV_MERGEABLE = 12,
    MADV_UNMERGEABLE = 13,
    MADV_HUGEPAGE = 14,
    MADV_NOHUGEPAGE = 15,
    MADV_DONTDUMP = 16,
    MADV_DODUMP = 17,
}

/// Give the system advice about the intended use of a memory region.
///
/// Unsafe because it is possible to zero memory with `MADV_REMOVE`.
pub unsafe fn madvise(range: &[u8], advice: MadviseAdvice) {
    let res = syscall3(NR_MADVISE, range.as_ptr() as u64, range.len() as u64, advice as u64) as c_int;
    if res != 0 {
        panic!("madvise failed with status {}", res);
    }
}

// === File operations ===

bitflags! {
    pub flags MemfdFlags: ::ctypes::c_int {
        const MFD_CLOEXEC = 1,
        const MFD_ALLOW_SEALING = 2,
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct FileHandle(c_int);

impl FileHandle {
    pub const unsafe fn from_fd(fd: c_int) -> FileHandle {
        FileHandle(fd)
    }
}

impl Into<isize> for FileHandle {
    fn into(self) -> isize {
        self.0 as isize
    }
}

/// Create a file which exists only in memory.
///
/// Used with `mmap` to get multiple VAs pointed to the same memory. Requires
/// Linux 3.17 or newer.
pub fn memfd_create(name: &[u8], flags: MemfdFlags) -> FileHandle {
    assert!(name.last() == Some(&0), "syscall filename must be null-terminated");
    FileHandle(unsafe {
        syscall2(NR_MEMFD_CREATE, name.as_ptr() as u64, flags.bits() as u64) as c_int
    })
}

/// Set the size of an open file in bytes.
pub fn ftruncate(fd: FileHandle, length: usize) {
    let res = unsafe {
        syscall2(NR_FTRUNCATE, fd.0 as u64, length as off_t as u64) as c_int
    };
    if res != 0 {
        panic!("ftruncate failed with status {}", res);
    }
}

/// Write bytes to an open file.
///
/// Returns the number of bytes actually written.
pub fn write(fd: FileHandle, buf: &[u8]) -> usize {
    let res = unsafe {
        syscall3(NR_WRITE, fd.0 as u64, buf.as_ptr() as u64, buf.len() as u64) as ssize_t
    };
    if res < 0 {
        panic!("write failed with status {}", res);
    }
    res as usize
}

// === Miscellaneous ===

pub fn exit(code: i32) -> ! {
    unsafe {
        syscall1(NR_EXIT, code as u64);
        ::core::intrinsics::unreachable();
    }
}

// === Signal handling ===

#[repr(C)]
pub struct rt_sigframe {
    // Omit because rsp at rt_sigreturn is assumed to be at sig
    //pretcode: *mut u8,
    sig: c_int,
    pinfo: *mut siginfo_t,
    puc: *mut u8,
    info: siginfo_t,
    uc: ucontext_t,
    _fpstate: fpstate_t,
    retcode: [u8; 8]
}

#[repr(C)]
pub struct sigaction_t {
    /// Function called on delivery of a signal.
    ///
    /// If an alternate signal stack is specified, this function is called on
    /// that stack.
    ///
    /// The second parameter is part of the kernel interface, but is generally
    /// not exposed to userspace. We probably don't care about its contents.
    pub sa_handler: extern "C" fn(c_int, *mut siginfo_t, *mut ucontext_t),
    pub sa_flags: c_int,
    /// Function called when `sa_handler` returns.
    ///
    /// On some architectures this function is optional and the kernel will
    /// place an appropriate trampoline somewhere in memory and arrange for
    /// it to be called when the handler returns. On others (such as x86_64),
    /// this is required and omitting it (setting to null) will raise SIGSEGV
    /// in response to the associated `sigaction` call.
    ///
    /// This function basically just needs to call the `sigreturn` syscall with
    /// the stack in the same state as it was on signal entry, less the return
    /// address on the stack which is popped when returning from the handler.
    pub sa_restorer: unsafe extern "C" fn() -> !,
    pub sa_mask: sigset_t,
}

// TODO verify this against kernel interface
#[repr(C)]
pub struct siginfo_t {
    /// Signal number
    pub si_signo: c_int,
    /// Error code associated with signal
    pub si_errno: c_int,
    /// Signal code (meaning varies with signal number)
    pub si_code: c_int,
    // Defined as a union following. The only time we care about this data is
    // for handling SIGSEGV, so the other variants are omitted. In that case
    // si_addr is the fault address.
    pub si_addr: *mut (),
}

/// Signal userland state
#[repr(C)]
pub struct ucontext_t {
    pub uc_flags: c_ulong,
    pub uc_link: *mut ucontext_t,
    pub uc_stack: stack_t,
    pub uc_mcontext: mcontext_t,
    pub uc_sigmask: sigset_t,
}

#[repr(C)]
pub struct stack_t {
    pub ss_sp: *mut u8,
    pub ss_flags: c_int,
    pub ss_size: size_t
}

/// Signal machine state
#[repr(C)]
pub struct mcontext_t {
    pub r8: u64,
    pub r9: u64,
    pub r10: u64,
    pub r11: u64,
    pub r12: u64,
    pub r13: u64,
    pub r14: u64,
    pub r15: u64,
    pub rdi: u64,
    pub rsi: u64,
    pub rbp: u64,
    pub rbx: u64,
    pub rdx: u64,
    pub rax: u64,
    pub rcx: u64,
    pub rsp: u64,
    pub rip: u64,
    pub eflags: u64,
    // Kernel only saves CS on x86_64; other segments are application-managed
    // if needed.
    pub cs: u16,
    pub gs: u16,
    pub fs: u16,
    __pad: u16,
    /// Error code from something
    pub err: u64,
    /// Dunno
    pub trapno: u64,
    /// Old signal mask
    pub oldmask: u64,
    pub cr2: u64,
    /// NULL if thread has no floating-point context
    pub fpregs: *mut fpstate_t,
    __reserved1: [c_ulonglong; 8]
}

/// Equivalent to the format for hardware FXSAVE.
pub type fpstate_t = [u8; 512];

/// One bit for each possible signal.
///
/// The userspace API differs from the actual syscall ABI here.
#[repr(C)]
#[derive(Debug)]
pub struct sigset_t(u64);

impl sigset_t {
    pub fn empty() -> sigset_t {
        sigset_t(0)
    }
}

bitflags! {
    pub flags SigactionFlags: ::ctypes::c_int {
        /// Pass siginfo in the signal stack frame
        const SA_SIGINFO = 4,
        /// Userspace will provide its own return handler (`sa_restorer`)
        ///
        /// This option is required on some systems.
        const SA_RESTORER = 0x04000000,
        /// Use alternate signal stack as registered by `sigaltstack`.
        const SA_ONSTACK = 0x08000000,
        /// Automatically restart interrupted system calls (don't return EINTR).
        ///
        /// Does not apply to all syscalls.
        const SA_RESTART = 0x10000000,
    }
}

/// Handle signal with default action
///
/// The returned function is not safe to call. It should only be used for
/// fields of `sigaction_t`.
#[allow(non_snake_case)]
pub unsafe fn SIG_DFL() -> extern "C" fn(c_int, *mut siginfo_t, *mut ucontext_t) {
    mem::transmute(ptr::null::<()>())
}

// Actually a c_int, but we turn this into a u64 for the syscall wrapper anyway.
#[repr(i8)]
pub enum Signal {
    SIGHUP = 1,
    SIGINT = 2,
    SIGQUIT = 3,
    SIGILL = 4,
    SIGABRT = 6,
    SIGFPE = 8,
    SIGKILL = 9,
    SIGSEGV = 11,
    SIGALRM = 14,
    SIGTERM = 15,
    SIGVTALRM = 26,
}

/// Send a signal to a process
pub fn kill(pid: c_int, sig: Signal) {
    let res = unsafe {
        syscall2(NR_KILL, pid as u64, sig as c_int as u64)
    };
    if res != 0 {
        panic!("kill returned status {}", res);
    }
}

/// Set the action to be taken on reception of a given signal.
// On x86-64 this is rt_sigaction which has no userspace API.
pub fn sigaction(signal: Signal, act: &sigaction_t) {
    let setsize = ::core::mem::size_of::<sigset_t>() as u64;
    let res = unsafe {
        syscall4(NR_SIGACTION, signal as u64, act as *const _ as u64,
                 ptr::null_mut::<sigaction_t>() as u64, setsize) as c_int
    };
    if res != 0 {
        panic!("sigaction returned status {}", res);
    }
}

pub fn sigaltstack(ss: &stack_t) {
    let res = unsafe {
        syscall2(NR_SIGALTSTACK, ss as *const _ as u64,
                 ptr::null_mut::<stack_t>() as u64) as c_int
    };
    if res != 0 {
        panic!("sigaltstack returned status {}", res);
    }
}

#[derive(Clone, Copy, Debug)]
pub enum ArchPrctlOperation {
    SetFS(*mut u8),
    GetFS,
    SetGS(*mut u8),
    GetGS
}

const ARCH_SET_GS: c_int = 0x1001;
const ARCH_SET_FS: c_int = 0x1002;
const ARCH_GET_FS: c_int = 0x1003;
const ARCH_GET_GS: c_int = 0x1004;

pub fn arch_prctl(operation: ArchPrctlOperation) -> Option<*mut u8> {
    use self::ArchPrctlOperation::*;

    let mut out: *mut u8 = ptr::null_mut();
    let (opcode, value) = match operation {
        SetFS(p) => (ARCH_SET_FS, p as u64),
        GetFS => (ARCH_GET_FS, &mut out as *mut _ as u64),
        SetGS(p) => (ARCH_SET_GS, p as u64),
        GetGS => (ARCH_GET_GS, &mut out as *mut _ as u64)
    };

    let res = unsafe {
        syscall2(NR_ARCH_PRCTL, opcode as u64, value) as c_int
    };
    if res != 0 {
        panic!("arch_prctl returned status {}", res);
    }

    match operation {
        GetFS | GetGS => Some(out),
        _ => None
    }
}

pub fn getpid() -> c_int {
    unsafe {
        syscall0(NR_GETPID) as c_int
    }
}
