use ctypes::c_int;
use core::{mem, ptr, slice};
use alloc::Allocator;
//use rkos_thread::{self as thread, Tcb, TcbHandle, current_stack_size};

use super::super::{
    INTERRUPT_STACK_TOP, INTERRUPT_STACK_SIZE,
    map_va_range
};
use super::syscall::{
    self,
    Signal, siginfo_t, ucontext_t,
    SA_ONSTACK, SA_RESTART, SA_RESTORER, SA_SIGINFO
};

pub type PageFaultHandler = fn(*mut u8, &mut PreemptState) -> bool;

/// Call `initializer` with a parameter on a new stack, which on return will
/// switch to the given `PreemptState`.
pub unsafe fn begin_threading<A: Allocator, T>(initializer: fn(T, &mut PreemptState), mut init_value: T,
                                               pf_handler: PageFaultHandler, alloc: A) {
    // Register pagefault handler
    PAGEFAULT_HANDLER = pf_handler;
    
    // Allocate some memory for the signal stack, map it in.
    // Must be page-aligned so we can mmap it.
    let stack_mem = slice::from_raw_parts_mut(alloc.allocate(INTERRUPT_STACK_SIZE, 4096),
                                              INTERRUPT_STACK_SIZE);
    debug!("Allocated signal stack ({:#x} bytes) at {:p}", stack_mem.len(), stack_mem.as_ptr());

    let stack_mapping = {
        let len = stack_mem.len();
        debug!("mapping {:#x} bytes from {:p} at {:#x} for interrupt stack", len, stack_mem, INTERRUPT_STACK_TOP - len);
        map_va_range(stack_mem, (INTERRUPT_STACK_TOP - len) as *mut u8)
    };
    debug!("Mapped signal stack at {:p}", stack_mapping.as_ptr());

    // Interrupts (signals) will use the new signal stack
    syscall::sigaltstack(&syscall::stack_t {
        ss_sp: stack_mapping.as_mut_ptr(),
        ss_flags: 0,
        ss_size: stack_mapping.len()
    });

    // Register our handler for SIGSEGV so we can handle page faults on stacks.
    syscall::sigaction(Signal::SIGSEGV, &syscall::sigaction_t {
        sa_handler: platform_pagefault_handler,
        sa_flags: (SA_RESTORER | SA_SIGINFO | SA_ONSTACK).bits(),
        sa_restorer: do_sigreturn,
        sa_mask: syscall::sigset_t::empty()
    });

    // Fire a signal so we can later sigreturn to the new thread.  Our current stack will remain
    // valid after this switch so we can move the allocator out, but we need to pass a pointer to
    // the allocator and the target function into the signal handler. We'll pull those out of the
    // saved state received by the signal handler, but they need to be in predictable registers.
    //
    // We could directly sigreturn into the new thread, but it's easier to fire a signal first
    // so there's already a valid signal frame to return to so we don't need to build one that
    // the kernel understands as valid.
    syscall::sigaction(Signal::SIGALRM, &syscall::sigaction_t {
        sa_handler: do_thread_init::<T>,
        sa_flags: (SA_RESTORER | SA_SIGINFO | SA_ONSTACK).bits(),
        sa_restorer: do_sigreturn,
        sa_mask: syscall::sigset_t::empty()
    });

    // Send ourselves a SIGALRM to actually call do_thread_init
    // kill(getpid(), SIGALRM) with necessary information in predictable registers.
    // Because we need to pass parameters to the signal handler, we cannot
    // use syscall::syscall2 and must duplicate its code.
    let out: i32;
    asm!("syscall"
         : "={eax}"(out)
         : "{rax}"(syscall::NR_KILL),
           "{edi}"(syscall::getpid()),
           "{esi}"(Signal::SIGALRM as i32),
           // Register values sniffed by signal handler as parameters
           "{r8}"(mem::transmute::<fn(T, &mut PreemptState), u64>(initializer)),
           "{r9}"(&mut init_value as *mut T as u64)
         : "rcx", "r11", "memory"
         : "volatile");
    if out != 0 {
        panic!("kill(self, SIGALRM) for do_thread_init failed: {}", out);
    }

    // Shouldn't happen unless the initializer fails to update the state
    // it receives.
    unreachable!();
    
    /// Sets up the new thread's state from the TCB after signal reception and returns into it.
    extern "C" fn do_thread_init<T>(signum: c_int, _: *mut siginfo_t, ctxt: *mut ucontext_t) {
        debug_assert!(signum == Signal::SIGALRM as c_int);

        let ctxt = unsafe {
            &mut *ctxt
        };
        let initializer = unsafe {
            mem::transmute::<u64, fn(T, &mut PreemptState)>(ctxt.uc_mcontext.r8)
        };
        let init_value = unsafe {
            ptr::read(ctxt.uc_mcontext.r9 as *mut T)
        };
        initializer(init_value, &mut PreemptState(ctxt));

        // Implicit sigreturn into the ucontext that was (presumably) updated
        // by the initializer function.
    }
}

pub struct PreemptState<'a>(&'a mut ucontext_t);

macro_rules! preempt_reg {
    ($reg:ident, $get:ident , $set:ident) => (
        #[inline]
        pub fn $get(&self) -> u64 { self.0.uc_mcontext.$reg }
        #[inline]
        pub fn $set(&mut self, x: u64) { self.0.uc_mcontext.$reg = x; }
    )
}
impl<'a> PreemptState<'a> {
    preempt_reg!(rax, get_rax, set_rax);
    preempt_reg!(rbx, get_rbx, set_rbx);
    preempt_reg!(rcx, get_rcx, set_rcx);
    preempt_reg!(rdx, get_rdx, set_rdx);
    preempt_reg!(rsi, get_rsi, set_rsi);
    preempt_reg!(rdi, get_rdi, set_rdi);
    preempt_reg!(rbp, get_rbp, set_rbp);
    preempt_reg!(rsp, get_rsp, set_rsp);
    preempt_reg!(r8, get_r8, set_r8);
    preempt_reg!(r9, get_r9, set_r9);
    preempt_reg!(r10, get_r10, set_r10);
    preempt_reg!(r11, get_r11, set_r11);
    preempt_reg!(r12, get_r12, set_r12);
    preempt_reg!(r13, get_r13, set_r13);
    preempt_reg!(r14, get_r14, set_r14);
    preempt_reg!(r15, get_r15, set_r15);

    preempt_reg!(rip, get_rip, set_rip);
    preempt_reg!(eflags, get_eflags, set_eflags);

    #[inline]
    pub fn get_fpstate(&self) -> Option<&[u8; 512]> {
        if self.0.uc_mcontext.fpregs.is_null() {
            None
        } else {
            Some(unsafe {
                &mut *self.0.uc_mcontext.fpregs
            })
        }
    }

    #[inline]
    pub fn get_fpstate_mut(&mut self) -> Option<&mut [u8; 512]> {
        if self.0.uc_mcontext.fpregs.is_null() {
            None
        } else {
            Some(unsafe {
                &mut *self.0.uc_mcontext.fpregs
            })
        }
    }
}

/// Set up the system to begin preemptive threading.
///
/// In short, start a periodic timer for thread (re)scheduling. We use SIGALRM
/// fired by a kernel interval timer. Each time that timer fires, we will
/// arrange to have the handler called with a handle for the machine state
/// at the time of preemption.
pub fn begin_preempt(handler: fn(&mut PreemptState)) {
    unsafe {
        PREEMPT_HANDLER = handler;
    }

    syscall::sigaction(Signal::SIGALRM, &syscall::sigaction_t {
        sa_handler: platform_preempt_handler,
        sa_flags: (SA_ONSTACK | SA_RESTART | SA_RESTORER | SA_SIGINFO).bits(),
        sa_restorer: do_sigreturn,
        sa_mask: syscall::sigset_t::empty()
    });

    // Start the interrupt timer
    syscall::setitimer(syscall::ITimerType::Real, &syscall::itimerval {
        it_interval: syscall::timeval {
            tv_sec: 10, // TODO machine::TIMER_INTERVAL or just delegate to something under machine::
            tv_usec: 0
        },
        it_value: syscall::timeval {
            tv_sec: 1,
            tv_usec: 0
        }
    });
}

static mut PREEMPT_HANDLER: fn(&mut PreemptState) = null_preempt_handler;
fn null_preempt_handler(_st: &mut PreemptState) {
    panic!("Preempted with no handler registered");
}

/// Handles preemption.
extern "C" fn platform_preempt_handler(signum: c_int, _: *mut siginfo_t, ctxt: *mut ucontext_t) {
    debug_assert!(signum == Signal::SIGALRM as c_int);

    use syscall::ArchPrctlOperation::GetGS;
    let gs = syscall::arch_prctl(GetGS).expect("GetGS should never return None");
    debug!("PREEMPT with GS (assumed TCB) = {:p}", gs);

    unsafe {
        let ctxt = &mut *ctxt;
        PREEMPT_HANDLER(&mut PreemptState(ctxt));
    }
}

extern "C" {
    pub fn do_sigreturn() -> !;
}
// TODO switch to this after testing things again, now that naked functions have landed.
/*
#[naked]
unsafe fn do_sigreturn() -> ! {
    // sys_sigreturn (sys_rt_sigreturn) just looks at our stack pointer to get
    // the frame back out, so we need a naked function.
    asm!("syscall"
         : /* No outputs (doesn't return) */
         : "{rax}"(__NR_sigreturn)
         : /* No clobbers */
         : "volatile");
    ::core::intrinsics::unreachable();
}
*/

static mut PAGEFAULT_HANDLER: PageFaultHandler = null_pf_handler;
fn null_pf_handler(_fault_addr: *mut u8, _st: &mut PreemptState) -> bool {
    debug!("Page fault with no handler registered");
    false
}

extern "C" fn platform_pagefault_handler(signum: c_int, info: *mut siginfo_t, ctxt: *mut ucontext_t) {
    debug_assert!(signum == Signal::SIGSEGV as c_int);

    // Fault address
    let fa = unsafe { (*info).si_addr as usize as *mut u8 };

    let handled = unsafe {
        let ctxt = &mut *ctxt;
        PAGEFAULT_HANDLER(fa, &mut PreemptState(ctxt))
    };

    if !handled {
        // Clear our SIGSEGV handler and rethrow
        syscall::sigaction(Signal::SIGSEGV, &syscall::sigaction_t {
            sa_handler: unsafe { syscall::SIG_DFL() },
            sa_flags: 0,
            sa_restorer: do_sigreturn,
            sa_mask: syscall::sigset_t::empty()
        });
    }
}

pub fn raise_preempt_interrupt() {
    use super::syscall::*;
    kill(getpid(), Signal::SIGALRM);
}
