#![no_std]
#![feature(asm, const_fn, core_intrinsics)]

#[macro_use]
extern crate bitflags;
extern crate ctypes;
#[macro_use]
extern crate rkos_core_macros;
extern crate rkos_alloc as alloc;

use core::slice;

/// Minimum size of a virtual memory page in bytes.
pub const VM_PAGE_SIZE: usize = 4096;
/// Top of interrupt stack mapping.
///
/// This mapping is maintained per-CPU, allocated from the global heap.
// 2 megabytes
pub const INTERRUPT_STACK_TOP: usize = 2 << 20;
/// Size of interrupt stacks
// 16 kilobytes
pub const INTERRUPT_STACK_SIZE: usize = 16 << 10;
/// Arbitrary maximum size of a thread's stack.
// 1 gigabyte
pub const THREAD_STACK_MAX_SIZE: usize = 1 << 30;

/// Base address of the global heap in virtual memory.
pub const HEAP_BASE: usize = 1 << 30;

/// Information about the running kernel image.
mod meta {
    // Symbols provided by the linker script. *pointers* to these statics are
    // the correct value.
    #[allow(non_upper_case_globals, improper_ctypes)]
    extern {
        static _sbss: ();
        static _ebss: ();
        static _kimage_start: ();
        static _kimage_end: ();
    }

    // TODO types on these should match the other address constants.
    pub static BSS_START: &'static () = &_sbss;
    pub static BSS_END: &'static () = &_ebss;
    pub static KERNEL_CODE_START: &'static () = &_kimage_start;
    pub static KERNEL_CODE_END: &'static () = &_kimage_end;
}

pub use meta::{
    BSS_START as BSS_BASE,
    BSS_END as BSS_TOP,
    KERNEL_CODE_START as KIMAGE_BASE,
    KERNEL_CODE_END as KIMAGE_TOP
};

/// Freestanding with Linux for hardware abstraction
#[cfg(all(target_os="linux", target_env=""))]
#[path="linux/mod.rs"]
mod imp;

/// Bare-metal
#[cfg(target_os="none")]
#[path="baremetal/mod.rs"]
mod imp;

/// Stubs for documentation.
///
/// This dummy implementation defines the required interfaces for a platform
/// module.
#[cfg(not(any(
    // Bare linux (freestanding with kernel for hardware abstraction)
    all(target_os="linux", target_env=""),
    // Bare-metal
    target_os="none"
)))]
#[path="stub/mod.rs"]
mod imp;

pub use imp::*;

pub fn ptr_is_page_aligned<T>(p: *const T) -> bool {
    let p = p as usize;
    debug_assert!(VM_PAGE_SIZE & (VM_PAGE_SIZE - 1) == 0, "VM page size assumed to be a power of two");
    (p & (VM_PAGE_SIZE - 1)) == 0
}

/// Map the physical memory backing the given `range` in at `target`, returning
/// a slice representing the new mapping.
///
/// Panics if any VA in the source range is not mapped or either source or
/// target is not at least page-aligned.
///
/// This does not unmap the source range, but will replace any existing
/// mappings within the target range.
pub unsafe fn map_va_range<'a>(source: &mut [u8], target: *mut u8) -> &'a mut [u8] {
    if !PhysPtr::va_range_is_mapped(source) {
        panic!("Part of map_va_range source region is not mapped");
    } else if !ptr_is_page_aligned(source.as_ptr()) || !ptr_is_page_aligned(target) {
        panic!("Source and target for map_va_range must be page-aligned")
    }

    let mut p = target;
    for (ptr, len) in PhysPtr::backing_regions(source) {
        ptr.map_at_va(p, len);
        p = p.offset(len as isize);
    }

    slice::from_raw_parts_mut(target, source.len())
}

pub fn get_timestamp() -> u64 {
    let hi: u64;
    let lo: u64;
    unsafe {
        asm!("rdtsc"
             : "={rdx}"(hi), "={rax}"(lo)
             : /* No inputs */
             : : "volatile");
    }
    hi << 32 | lo
}
